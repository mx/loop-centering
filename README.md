[![pipeline status](https://gitlab.psi.ch/mx/loop-centering/badges/master/pipeline.svg)](https://gitlab.psi.ch/mx/loop-centering/-/commits/master)

Automatic Loop Centering
========================

Automatic Loop Centering consist of the following components:

* Python Flask application implementing REST API to start loop centering jobs
    * one per beamline
    * runs on ```x06sa-cons-705:1328```, ```x10sa-cons-705:1328``` and ```x06da-cons-705:1328```
* Redis to control the state of loop centering
    * runs on ```{beamline}-cons-705:6379``` in docker container `alc-redis`
    * docker image can be obtained from PSI repo:```sudo docker pull docker.psi.ch:5000/mx/redis```
    * common for beamlines
* ```AlcClient.py``` - python client to use REST API. Part of ```mxlibs``` and ```mxlibs3``` packages.
* CI gitlab-runner workers and redis instance with exposed 6379 port are located on ```mx-webapps.psi.ch```

Logfiles and images
-------------------

* Systemd log file is stored in ```/var/log/journal```
* Systemd log file is accessible with ```ssh {psi_account}@{beamline}-cons-705 sudo journalctl -f -q -o short-precise -u alc```
* Log file is stored in ```/scratch/alc_data/alc-{beamline}.log```
* logfile is rotated with logrotate
* images are saved in ```/scratch/alc_data/alc_images```


Deploying and running ALC service
---------------------------------

* default port: 1328
* default location of log file:```/scratch/alc_data/```
* default location where ALC server stores camera images: ```/scratch/alc_data/images```
* ALC server requires REDIS key/value data storage to run. The REDIS location can be customized
  in ```src/etc/redis_conf.py```

* deploying in ```/sls/MX/applications```

```bash
$ make x06sa # to deploy for x06sa beamline
```

* starting/stopping

```bash
$ sudo systemctl start alc # start alc service

$ sudo systemctl stop alc # stop alc service
```

* when deploying on new machine see ```scripts``` directory for service start script than is put
  in ```/etc/systemd/system```

Organization of the repository
------------------------------

* ```src``` - application source code - Flask application (```app```), image processing and beamline motors
  interface (```lib```), centering workers (```{beamline}-workers```)
* ```scripts``` - scripts deployed in ```/etc/systemd/system``` to make loop-centering as a service
* ```tools``` - some scripts to troubleshoot loop-centering
* ```work``` - various junk scripts used to play with machine learning
* ```tests``` - early attempt to keep unit tests up to date....

Image Processing Methodology
----------------------------

* ```src/lib/loopImageProcessing.py``` - class implementing all image processing algorithms. It is used by ALC service,
  but can be also used standalone to process images from camera.

### Finding contours

```python
import loopImageProcessing as lip
import sampleCamera
import beamlineMotors

motors = beamlineMotors.beamlineMotors('X06SA')
# Get current zoom (zoom motor values)
current_zoom = motors.currentZoom()
# Get image
samcam = sampleCamera.sampleCamera('X06SA')
img = samcam.getImage()
proc_img = lip(img)

# Process image at current zoom level
proc_img.findContour(current_zoom, 'X06SA')

# Draw contour on image overlay save it to file
file_path = proc_img.showContour(save_img=False)

# You can also display contour. Note: may not work in iPython
proc_img.showContour()

```

Contour processing parameters are fixed for given zoom and for given beamline
in ```loopImageProcessing.py::__setOpenCVParams()```. Zoom value is given as zoom motor value.

To implement new contouring parameters for given beamline, microscope, zoom level make changes
in ```loopImageProcessing.py::__setOpenCVParams()```.

### Finding grid scan box

To find grid scan box, first find contour of the loop.

```python
img = samcam.getImage()
proc_img = lip(img)
# First - find contour
proc_img.findContour(current_zoom, 'X06SA')
# Find bounding box
box = proc_img.findLoopBoundingBox()

# box = {'top_left': np.array([x_t, y_t]), 'bottom_right': np.array([x_b, y_b])}

# Save contour image to file
file_path = proc_img.showLoopBoundingBox(save_img=True)
print(file_path)
```

Algorithm to find loop bounding box is performed with two different methods: ```rectangleFit``` and ```ellipseFit```.
The final box is the one that better fits the loop shape.

#### Finding box using rectangle (```loopImageProcessing.py::fitRectangle()```)

1. Find extremes of the loop: top-most-, bottom-most- and left-most
   points. (```loopImageProcessing.py::findExtremes()```)
2. Using coordinates of these points, find rectangle spanning over the
   loop (```loopImageProcessing.py::fitRectangle()```)
    * the height of the box is the distance between top-most and bottom-most-point
    * the width of the the box is the distance between left-most point and top-most or bottom-most point, which ever is
      bigger, multiplied by two.

#### Finding box using ellipse (```loopImageProcessing.py::fitEllipse()```)

1. Ellipse is fitted to the loop contour
2. Minimum box fitting over that ellipse is found

#### Choosing final box (```loopImageProcessing.py::findLoopBoundingBox()```)

```findLoopBoundingBox()``` calls both ```fitRectangle()``` and ```fitEllipse()``` methods and compares resulting boxes.
The better box is the one which right edge is closer to the image boundary.


ALC workers - ```src/{beamline}-workers```
------------------------------------------

Automatic Loop Centering is a service that via REST API runs worker threads on the server. Workers perform loop
centering tasks.

### Organization of worker threads

* workers are organized per beamline and stored in ```src/$BEAMLINE_XNAME-workers``` directory
* when starting the ALC server, workers from appropriate directory are imported based on BEAMLINE_XNAME environmental
  variable
* if BEAMLINE_XNAME is not set, workers from ```src/BOGUS-workers``` directory are imported

### Description of  workers

1. ```centerToFlat``` - centers loop to flat face. Uses ```centerOnTip```, ```findFlatFace```, ```findLoopThickness```
   and ```findLoopBoundingBox``` workers

2. ```centerOnTip``` - centers on the tip of the pin, zooms-in to the high maginfication
   (value given in Hub ```alc_zoom_at_end``` variable), and moves toward the loop center by the cooresponding to the
   beamsize
    * it does not guarantees that the loop will be cetnered face-on
    * all the moves are performed in the cryzo-zone

3. ```findFlatFace``` - finds flat face of the loop. Rotates Omega by OmegaStep, gets loop image and calculates area of
   loop contour. Performs interpolation to find the angle for which the contour area is the largest.

4. ```noCentering``` - does not perform loop centering with sample microscope camera.

5. ```samCamManual``` and ```samCamAuto```  - sets high exposure and gain on the microscope camera, and set exposure and
   gain to be set automatically respectively

6. ```testSetup``` - moves Prigo to the postion as it is set when mounting with robot (-18.0, 0.0 ,0.0)

7. ```rotateAndPic``` - rotates Omega by 180 in steps (default=10 deg) and takes a picture of the sample. Saved
   in ```/scratch/alc_data/images```

8. ```prelocate``` - initial step at X06SA and X10sa using the top camera to align sample in manual sample exchange to
   beam position. Once in sample alignment mode the above selected centering can occur; `noCentering`, `centerOnTip`,
   or `centerToFlat`

10. ```calibrateTopCamera``` - mapping the beam position and rotation axis from the microscope camera in sample
    alignment to the top camera in manual sample exchange. Part of standard beamline setup in DA+ GUI.

### Implementing new worker threads

Each worker thread is a subclass of ```src/lib/WorkersBaseClass.py```. ```workerBaseClass``` defines common interface
for all the workers:

* access to beamline devices

```python
self._r  # instance of redis driver
self.samCam  # instance of src/lib/sampleCamera.py
self.sampleMotor  # instance of src/lib/beamlineMotors.py::SampleMotor{beamline}
self.omegaMotor  # instance of src/lib/beamlineMotors.py::OmegaMotor
self.frontLight  # instance of src/lib/frontLight
self.backLight  # instance of src/lib/backLight
self.topCam  # instance of src/lib/topCamera
```

* implements decorator ```@worker_logic```
    * controls Redis state of running worker, updates Redis with current worker state
    * puts data returned by worker to Redis
    * catches Python Errors if occurred during worker run time, reposts them to Redis and adjust state of application
      accordingly

* ```src/X06DA-workers/workerTemplate.py``` - template how to implement new workers
* simple workers that are good example of implementation
    * ```samCamAuto.py``` and ```samCamManual.py```
    * ```centerOnTip.py```

* a worker can be used from the inside of other worker. For example see ```centerOnTip.py``` that among many others,
  calls```centerOnTip``` worker

* each worker is a Python Thread

Workers Engine - ```AlcDriver.py```
------------------------------------

```AlcDriver``` is the main class to start/stop/abort worker threads. It is initialized when starting Flask application.

The role of ```AlcDriver``` is:

* initialize beamline devices
* set initial Redis state
* start and abort workers

Redis
-----

Redis is used to store the current state of running ALC application. It defines two kinds of variables:

* ```globalStates``` - global state of the application
* ```workerRuntimeState``` - current state or running worker thread

### Redis configuration

Redis configuration is stored in ```etc/redis_conf.py```

### Redis **global** states

Redis global states ```src/lib/globalStates.py``` controls global state of application, specifying if

* application is busy, e.g. some worker is currently running
* abort commands was sent to worker
* name of currently running worker.

All the values are strings, ```src/lib/globalStates.py``` provides interface to control these values in uniform way.

### Redis **worker runtime** states

Opposed to global states, worker states stored in Redis correspond to the current state of the running worker. See the
implementation in ```src/lib/workerRuntimeStates.py```.

* ```workerName``` - name of a running worker
* ```workerResult``` - result of the worker - Success, Failed, Aborter, Error (python Exception)
* ```workerData``` - data structure, result returned by worker (if any)

All the values are strings, ```src/lib/workerRuntimeStates.py``` provides interface to control these values in uniform
way.


REST API
--------

### GET commands

* Check state if ALC is busy

  ```
  http://hostname:port/busy

  Response:
  {busy:<true|false>}
  ```

* Check status

  ```
  http://hostname:port/status

  Response:
  {busy:<true|false>,
   name:workerName,
   result:<Success|Failed|Aborted|Error>,
   error:errorMessage if any,
   data: {key:Object, key2: Object}}
  ```
    * when ```busy:false``` the command will return name and result of last running worker thread

* List available workers

  ```
  http://hostname:port/list

  Response
  {status:[list of worker names]}
  ```

### POST commands

* Abort currently running worker thread

  ```
  http://hostname:port/abort

  Response:
  HttpCode 200 - operation sucessful
  HttpCode 202 - could not change state to ABORT for some reason

  {status:"info if the ABORT was sucesfull or not"}
  ```

* Run worker thread

  ```
  http://hostname:port/run/<worker_name>

  Response:
  HttpCode 200 - operation sucessfull
  HttpCode 202 - other worker already running
  HttpCode 409 - couldn't run worker

  {status:"information about success or failure}
  ```

* Restart the server, kill all the worker threads

  ```
  http://hostname:port/abort


  Response:
  {status: OK}
  ```

ALC as a State Machine
======================
ALC server is a state machine, meaining, it will not allow any other worker thread run, if one is already running. It is
also thread safe, so only one request at the time will be able to lock the state and run the worker. The thread safety
is realized by running **REDIS** instance


# Top Camera Setup

This document describes how to choose Loop-Centering settings to work and troubleshot TopCam at X06SA & X10SA.


## Hub Variables controlling TopCam operations

```bash
"alc_topcam_beam_position_x": "40",
"alc_topcam_beam_position_y": "250",
"alc_topcam_start_angle": "-57",
```

### topcam_beam_position_{x,y}

These variables set the location of the beam in the TopCam coordinate system. They define the target position where
TopCam prelocation routine will move the tip of the loop.

To find these coordinates:

1. Mount pin on goniometer
2. In DA+GUI position the pin is such a way that the tip of the pin is in the center of beambox
3. Use the script ```tools/check-X06SA-topcam.py``` to find the tip of the loop.
4. The script will print location of the tip in the topcam coordinates. Use this values to set hub variables
    * ```alc_topcam_beam_position_x```
    * ```alc_topcam_beam_position_y```

Note: these coordinates does not need to be exactly set up to beambox location. It is sufficient to set them to the
middle of be sample microscope viewport at the given zoom.

Note: you might need to adjust those variables when microscope is moved.

### alc_topcam_start_angle

This setting is tricky. The goal is to align Goniometer coordinate system with TopCam coordinate system, i.e. Goniometer
axis Y is parallel to TopCam Y axis.

In such a way there is no need to implement Rotation matrix when moving GMY, GMZ when using TopCam coordinate system.

As of time of writing this document, the ballpark value is between -50 < Omega < -60 degrees.

There are following ways to get a ballpark value:

* using angle measure to measure the TopCam angle in the fixture
* put pin on gonio with some flat surface attached it (for instance PostIt attached to pin) and rotate Omega eyeballing
  when the surface is aligned with TopCam
* others....

**To fine tune the angle done the following procedure can be used:**

* put pin on gonio and rotate Omega to ballpark value.
* Move GMZ from OnePanel and observe view from TopCam. We are looking for Omega angle in which moving GMZ does not
  change position of the pin in the TopCam view.
* If you see movement of the Pin in TopCam view when changing GMZ, adjust Omega and repeat.
* When correct omega angle is found, moving GMZ will not change position of the pin, and moving GMY will move the pin up
  and down in TopCam view.
* set  ```alc_topcam_start_angle``` to this Omega values

### TopCam pixel to mm conversion

Pixel to mm conversion for TopCam is set in ```src/lib/topCamera.py``` class

To find the conversion:

* put pin on gonio, use ```tools/check-X06SA-topcam.py``` to find the tip of the loop.
* move GMX by know value, for example 1mm and find the new position on the tip.
* Calculate conversion from difference of ```x``` coordinates of the tip
* Since TopCam is not super accurate, you might repeat the measurement few times to see the spread

### Image ROI

On X06SA cryojet is in the Topcam view. To successfully detect loop features the cryojet is cropped from the image. The
amount of cropping is adjusted by ```roiDimensions``` variable in ```src/loopImageProcessing.py``` for TopCam. In
practice, whenever TopCam is moved its good to check if the values in ```roiDimensions``` variable make sense.

# ALC Hub parameters

```
      # Zoom parameters for microscope sample camera
      "alc_initial_zoom": "400"
      "alc_zoom_at_end": "600"

      # Logic check for if ALC is running at beamline
      "service_alc_enabled": "true" <- teller.py
      "service_alc_implemented": "true" <- escapewe

      # Position of cryo at beginning of prelocate
      "alc_cryo_initial": "8.0"

      # Adjustable final movement after centerToFlat in hub
      "alc_final_movement_mm": "-0.18"

      # Values set manually after every shutdown
      "alc_samcam_gain": "0"
      "alc_samcam_exposure": "0.004"

      # In DA+ GUI advanced options
      "alc_prelocate": "false"
      "alc_centering_job": "noCentering"
      "alc_available_jobs": "[\"noCentering\",\"centerToFlat\",\"centerOnTip\"]"

      # Currently set manually, moved to mxconfig.yaml need to update clients
      "alc_service_endpoint": "http://x10sa-cons-705.psi.ch:1328" <- DA+ GUI, AlcClient

      # Set automatically during beamline set up with calibrateTopCamera worker
      "alc_topcam_start_angle": "-66"
      "alc_topcam_beam_position_x": "137"
      "alc_topcam_beam_position_y": "236"

```

