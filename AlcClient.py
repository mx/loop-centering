import json
import os
import requests
from loguru import logger

from mx_preferences import get_config


def request_handler(requests_call):
    """Wraps try/except statements around call to the requests module."""

    def wrapper(*args, **kwargs):
        try:
            self = args[0]
            wrapped = requests_call(*args, **kwargs)
        except requests.ConnectionError:
            raise RuntimeError("Cannot connect to the ALC server at {}".format(self.alc_server))
        except requests.ReadTimeout:
            raise RuntimeError("Connection to ALC server at {} timeout".format(self.alc_server))
        return wrapped

    return wrapper


class AlcClient(object):
    """Client for Automatic Loop Centering service at SLS MX beamlines.

    Python client for connecting to an ALC server. Returns the following result values: `True`, `False` and "Success".

    Example:
    >>> alc = AlcClient.AlcClient()

    Attributes:
        __bl (str): beamline the alc server is running.
        alc_server (str, optional): host url and port where alc server is running. default is http://127.0.0.1:5000

    """

    def __init__(self, beamline=None):

        beamline_list = ["x06da", "x06sa", "x10sa", "bogus"]
        if beamline is None:
            beamline = os.getenv("BEAMLINE_XNAME", "bogus").lower()

        if beamline not in beamline_list:
            raise Exception("Beamline needs to be one of {}.".format(str(beamline_list)))

        self.__bl = beamline

        try:
            self.alc_server = "http://" + get_config(beamline)["alc"]["host"] + ":1328"
        except Exception as e:
            self.alc_server = "http://127.0.0.1:5000"
            logger.info("Exception: {}. Setting alc server address to {}".format(e, self.alc_server))

    @request_handler
    def __get(self, route, timeout=10):
        return requests.get(route, timeout=timeout)

    @request_handler
    def __post(self, route, data=None, timeout=10):
        if data is None:
            data = {}
        return requests.post(route, json=data, timeout=timeout)

    @staticmethod
    def __error_handler(r, route):
        """Throws a Runtime error if REST return code != 200.

        Args:
            r: an instance of requests module
            route: name of the asked route

        Raises:
            Runtime error if REST return code != 200

        """
        if r.status_code != 200:
            raise RuntimeError(
                'Cannot retrieve status "{}" from the ALC server. Rest error code: {}, Message: {}'.format(
                    route, r.status_code, r.text
                )
            )

    def __kwargs2URL(self, **kwargs):
        """Converts kwargs to ?key1=val1&key2=val2... If kwargs are empty, returns empty string."""
        s = "&".join(["{}={}".format(k, v) for k, v in kwargs.items()])
        if s:
            s = "?" + s
        return s

    def busy(self):
        """Checks if ALC is busy."""
        r = self.__get(self.alc_server + "/busy")

        self.__error_handler(r, "busy")

        text = json.loads(r.text)

        try:
            busy = text["busy"]
        except KeyError:
            raise RuntimeError('ALC Client Error: key "busy" does not exist in the response from the ALC server')

        return busy == True

    def abort(self):
        """Aborts currently running ALC job."""
        logger.info("ALC - Issuing ABORT command to ALC worker...")
        r = self.__post(self.alc_server + "/abort")

        if r.status_code == 200:
            logger.info("ALC - ABORT command issued to ALC worker.")
            return True
        elif r.status_code == 202:  # Nothing to Abort or Abort is already on the way
            logger.info(r.text)
            return False
        else:
            raise RuntimeError("ALC - Could not ABORT ALC worker: {}".format(r.text))

    def status(self):
        """Returns status of current activity on ALC server.

        Returns:
            json: response received from alc server. For instance:

        ```python
        {
        busy: True/False,
        name:"name_of_last_worker",
        result: "Success"|"Failed"|"Aborted"|"Error",
        error:"Stacktrace if error",
        data:{dict with data from workers}
        }
        ```

        e.g.

        ```python
        {'busy': False,
        'data': {'worker_out': {'motor_x_at_tip': 18.301133612088403,
        'motor_y_at_tip': -0.17730386168913045,
        'motor_z_at_tip': -0.15616926468582762,
        'omega': 430.009,
        'tip_x': 710.2025395909087,
        'tip_y': 468.63009894911113}},
        'error': 'None',
        'name': 'centerOnTip',
        'result': 'Success'}

        ```

        Raises:
            TypeError: no json response from alc server received.

        """
        r = self.__get(self.alc_server + "/status")

        self.__error_handler(r, "status")
        """:returns: json.loads(r.text)"""
        resp = r.json()
        try:
            resp["data"] = json.loads(resp["data"])
        except TypeError:
            pass
        return resp

    def list(self):
        """List available ALC worker jobs.

        Returns:
            list: available loop centering worker jobs at current beamline.

        ```python
        {'status': ['calibrateTopCamera',
        'centerOnTip',
        'centerToFlat',
        'findFlatFace',
        'findLoopBoundingBox',
        'findLoopThickness',
        'noCentering',
        'prelocate',
        'samCamAuto',
        'samCamManual',
        'saveCurrentImage',
        'saveRasterGrid',
        'workerTemplate']}

        ```

        """
        r = self.__get(self.alc_server + "/list")

        self.__error_handler(r, "status")
        return json.loads(r.text)

    def restart(self):
        """Restart ALC server and kill all currently running workers."""
        return self.__post(self.alc_server + "/restart")

    def run_job(self, worker_name, **kwargs):
        """Run selected job on ALC server.

        The list of available jobs can be found with AlcClient.list()
        Convert kwargs to ?key1=val1&key2=val2...kwargs = self.__kwargs2URL(**kwargs)

        Args:
            worker_name (str): for instance: alc.run_job('centerOnTip')
            **kwargs (optional): additional arguments that will be passed to the ALC worker.

        Returns:
            str: acknowledgement if alc worker has been started.

        """

        logger.info("ALC - Running worker: {}".format(worker_name))

        r = self.__post(self.alc_server + "/run/" + worker_name, data=kwargs)

        if r.status_code == 200:
            logger.info("ALC - Worker {} started".format(worker_name))
            return True
        elif r.status_code == 202:
            logger.info("ALC - Worker {} not started. Reason: {}".format(worker_name, r.text))
            return False
        else:
            raise RuntimeError("ALC - Could not run worker. REST error code: {} {}".format(r.status_code, r.text))

    def saveCameraImage(self, user, directory, filename, grid_lines=None, draw_overlay=False):
        """Save microscope camera image to provided directory/filename.

        Saves current microscope camera image to path given by directory/filename in user home directory.
        Writes the images changing the user privileges to `user`

        Args:
            user: user for whom write the image file. Can be e-account
            directory: absolute directory where to write the file, for instance '/sls/X06SA/data/e15880/Data10/test'
            filename: name of the file, with extension. For instance test.jpg
            grid_lines: optional, list of lists of grid lines coordinates from RasterGridGenerator.grid_lines().
            It will additionally draw grid lines on image
            draw_overlay (bool): optional, watermark image with beamline, beam size and beam location.

        Returns:
            json: alc.status() message containing full path where image was saved.

        ```python
        {
        'busy': False,
        'data': '{"worker_out": {"savedImagePath": "/sls/X06SA/data/e15880/Data10/test/test_from_alc.jpg"}}',
        'error': 'None',
        'name': 'saveCurrentImage',
        'result': 'Success'
        }
        ```

        """
        if grid_lines is None:
            grid_lines = []
        out = self.run_job(
            "saveCurrentImage",
            setupSamCam=False,
            target_dir=directory,
            target_filename=filename,
            user=user,
            grid_lines=grid_lines,
            draw_overlay=draw_overlay,
        )
        return self.status()

    def saveRasterGrid(
            self,
            user,
            directory,
            filename,
            raster_result,
            grid_pixel_positions,
            grid_cell_width_px,
            grid_cell_height_px,
            max_value,
            max_value_position,
            grid_lines=[],
            draw_beambox=False,
    ):
        """Overlays raster scan result on sample microscope image.

        Overlays result of raster scan on current image of sample from microscope.
        The images are written with user privileges given by 'user'.

        Args:
            user (str): user for whom write the image file. Can be e-account
            directory (str): absolute directory where to write the file, e.g. '/sls/X06SA/data/e15880/Data10/test'
            filename (str): name of the file, with extension. For instance test.jpg
            raster_result (np.array): Result of raster scan
            grid_pixel_positions (list of lists): grid positions, in pixels
            grid_cell_width_px (int): width of one grid box, in pixels
            grid_cell_height_px (int): height of one grid box, in pixels
            max_value (int, optional): maximum spot count value in SDU determined best grid position.
            max_value_position (list, optional): co-ordinates in pixels of SDU determined best grid position.
            grid_lines (list of lists, optional): overlay of grid lines to be additionally drawn on image.
            draw_beambox (bool, optional): if `True` a beambox will be drawn on the image

        Returns:
            json: AlcClient.status() message containing full path where image was saved. For instance:

        ```python
        {
        'busy': False,
        'data': '{"worker_out": {"saveRasterGrid": "/sls/X06SA/data/e15880/Data10/test/test_from_alc.jpg"}}',
        'error': 'None',
        'name': 'saveCurrentImage',
        'result': 'Success'
        }
        ```

        """
        out = self.run_job(
            "saveRasterGrid",
            setupSamCam=False,
            target_dir=directory,
            target_filename=filename,
            user=user,
            raster_result=raster_result.tolist(),
            grid_pixel_positions=grid_pixel_positions,
            grid_cell_width_px=grid_cell_width_px,
            grid_cell_height_px=grid_cell_height_px,
            max_value=max_value,
            max_value_position=max_value_position,
            grid_lines=grid_lines,
            draw_beambox=draw_beambox,
        )

        return self.status()
