# Moving ALC service to a new machine

First, become root on the machine you want to deploy the ALC service on.

```
sudo su -
```

Update the HOST variable to the new machine e.g.

```
hub alc_service_endpoint=x06sa-cons-705
```

Place ```alc.service``` script in `/etc/systemd/system/`

```
scp /sls/MX/applications/git/loop-centering/scripts/alc.service /etc/systemd/system/
```

Update ```alc.service``` script ```beamline_xname``` variable to x06sa, x06da or x10sa respectively.

```
WorkingDirectory=/sls/MX/applications/loop-centering/beamline_lower/stable
```

Update ```alc.service``` script `BEAMLINE_UPPER` variable to X06SA, X06DA or X10SA respectively.

```
ExecStart=/usr/bin/bash ./alc_launcher.sh X10SA
```

Reload systemctl

```
systemctl daemon-reload
```

Make folders for ALC logs and images to be saved to:

```
mkdir /scratch/alc_data
mkdir /scratch/alc_data/alc_images
```
