#!/bin/bash

. /opt/gfa/python
source activate /sls/MX/applications/conda_envs/alc


export EPICS_CA_MAX_ARRAY_BYTES=40000000 # required to run from root account


if [[ -n $BEAMLINE_XNAME ]] ; then
    beamline=$(echo $BEAMLINE_XNAME | tr A-Z a-z)
elif [[ -n $PUPPET_GROUP ]] ; then
    beamline=$(echo $PUPPET_GROUP | tr A-Z a-z)
else
    echo "unknown beamline/environment"
    exit 1
fi

M=/sls/MX/applications

shopt -s nocasematch
case "X_$beamline" in
    x_x06sa )
        export PYTHONPATH=${M}/mxlibs3/all-beamlines/stable/:${M}/loop-centering/x06sa/stable/lib
        echo "PYTHONPATH=$PYTHONPATH"
        python /sls/MX/applications/loop-centering/check_topcam.py X06SA;
        ;;
    x_x10sa )
        export PYTHONPATH=${M}/mxlibs3/all-beamlines/stable/:${M}/loop-centering/x10sa/stable/lib
        echo "PYTHONPATH=$PYTHONPATH"
        python /sls/MX/applications/loop-centering/check_topcam.py X10SA;
        ;;
    * )
        echo "beamline ${beamline} top camera is not implemented"
        ;;
esac
