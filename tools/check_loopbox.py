# To run in terminal use conda env python to run script
# /sls/MX/applications/conda_envs/alc/bin/python check_loopbox.py

import sys

sys.path.insert(0, "/sls/MX/applications/git/loop-centering/src/lib/")
sys.path.insert(0, "/sls/MX/applications/mxlibs3/all-beamlines/stable/")

import AlcClient
import beamlineUtils as bu
import click
import hubclient
import os
import re
import time
from loopImageProcessing import loopImageProcessing as lip

alc = AlcClient.AlcClient()


@click.command()
@click.option("--beamline", "-b", help="Please provide beamline")
@click.option("--input_dir", "-i", help="Please provide path to input directory with trailing /.")
@click.option("--output_dir", "-o", help="Please provide path to output directory with trailing /.")
def check_loopbox(beamline, input_dir, output_dir):
    print("{}, {}, {}".format(beamline, input_dir, output_dir))
    beamline = beamline.upper()
    if beamline == "X06SA":
        zoom = 4.0
    elif beamline == "X10SA":
        zoom = 400.0
    elif beamline == "X06DA":
        zoom = -208.0

    images = [f for f in os.listdir(input_dir) if re.match(r"[0-9]+.*\.jpg", f)]

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    for image in images:
        print(image)
        img = input_dir + image
        procImg = lip(img)
        procImg.findContour(zoom=zoom, beamline="params{}bounding".format(beamline), hull=False)
        box = procImg.findLoopBoundingBox()
        procImg.showLoopBoundingBox(
            draw_rectangle_boxes=True, draw_ellipse_boxes=False, save_img=True, directory=output_dir
        )
        time.sleep(1.0)


if __name__ == "__main__":
    check_loopbox()
