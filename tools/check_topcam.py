#!/usr/bin/env python

# This script will find the Tip of the loop from TopCam and show TopCam image cropped
# to the Region-Of-Interest for processing, i.e. no croyjet in view.

import backLight
import frontLight
import hubclient
import os
import sys
import time
import topCamera
from loguru import logger
from loopImageProcessing import loopImageProcessing as lip

if os.uname()[1] not in ["x06sa-cons-703.psi.ch", "x10sa-cons-703.psi.ch"]:
    print("You must be on beamline console 703 to run this utility.")
    print("Your current location is: {}".format(os.uname()[1]))
    sys.exit(1)

beamline = sys.argv[1]
hub = hubclient.Hub(beamline.upper())
bl = backLight.backLight(beamline=beamline)
fl = frontLight.frontLight(beamline=beamline)
topcam = topCamera.topCamera(beamline=beamline)

LOG_FILENAME = "/scratch/alc_topcam/{}_topcam.log".format(beamline, beamline)
logger.add(LOG_FILENAME, level="INFO", rotation="100MB")

# Ensure hardware has correct settings for image capture
bl.setup()  # back light off and lamp up
fl.setup()  # front light to full
# TODO: move cryojet to 12mm

# Wait for lighting and then capture image
time.sleep(2.0)
img = topcam.getImage()

# Return hardware to correct setting for sample alignment
bl.restore()  # back light to full
fl.restore()  # front light off
# TODO: move cryojet back to 5mm

# Get the current beam position for top camera
old_x = hub.get("alc_topcam_beam_position_x")
old_y = hub.get("alc_topcam_beam_position_y")
logger.info("Currently stored Tip location [{} {}]".format(old_x, old_y))

# Process image, apply ROI, find tip
procImg = lip(img)
procImg.findContour(zoom="topcam", beamline=beamline, hull=True)
tip = procImg.findTip()
logger.info("Newly determined Tip location {}".format(tip))

notify = (
    str(input("Would you like to update the Tip location for beam position on top camera to new values - yes or no?: "))
    .lower()
    .strip()
)
if notify == "yes":
    hub.put("alc_topcam_beam_position_x", tip[0])
    hub.put("alc_topcam_beam_position_y", tip[1])
    logger.info("You chose to update top camera tip location for beam position values: {}".format(tip))
elif notify == "no":
    logger.info("You chose to keep current top camera tip location for beam position : [{} {}]".format(old_x, old_y))

# logger.info("Saving image to: /sls/MX/applications/git/loop-centering/tools/{}_check_topcam".format(beamline))
# procImg.saveImage("/sls/MX/applications/git/loop-centering/tools/{}_check_topcam".format(beamline))
