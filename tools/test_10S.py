#!/usr/bin/env python

# This script will find the Tip of the loop from TopCam and show TopCam image cropped
# to the Region-Of-Interest for processing, i.e. no croyjet in view.

import sys

sys.path.insert(0, "/sls/MX/applications/git/loop-centering/src")
sys.path.insert(0, "/sls/MX/applications/git/loop-centering/src/lib")
sys.path.insert(0, "/sls/MX/applications/git/mxlibs3/src")

# import backLight
# import frontLight
import hubclient
import os
import sys
import time
import topCamera
from loopImageProcessing import loopImageProcessing as lip

beamline = "X10SA"
# hub = hubclient.Hub(beamline.upper())
# bl = backLight.backLight(beamline=beamline)
# fl = frontLight.frontLight(beamline=beamline)
topcam = topCamera.topCamera(beamline=beamline)

# Ensure hardware has correct settings for image capture
# bl.setup()  # back light off and lamp up
# fl.setup()  # front light to full

# Wait for lighting and then capture image
img = topcam.getImage()

# Return hardware to correct setting for sample alignment
# bl.restore()  # back light to full
# fl.restore()  # front light off
# TODO: move cryojet back to 5mm

# Get the current beam position for top camera
# old_x = hub.get("alc_topcam_beam_position_x")
# old_y = hub.get("alc_topcam_beam_position_y")
# print("Currently stored Tip location [{} {}]".format(old_x, old_y))

# Process image, apply ROI, find tip
procImg = lip(img)
procImg.findContour(zoom="topcam", beamline=beamline, hull=True)
tip = procImg.findTip()
print("Newly determined Tip location {}".format(tip))
procImg.showTip()
print("Saving image to: /sls/MX/applications/git/loop-centering/tools/{}_test_topcam".format(beamline))
procImg.saveImage("/sls/MX/applications/git/loop-centering/tools/{}_test_topcam".format(beamline))
