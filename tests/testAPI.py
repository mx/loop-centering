import os
import sys
import unittest

sys.path.insert(0, "src/lib")
import globalStates as state
import workerRuntimeStates as workerRuntime
import time
import redis
import requests
import json


# Note: make sure REST LOCAL! server and REDIS are running


class testAPI(unittest.TestCase):
    def setUp(self):
        redis_host = os.getenv("ALC_REDIS_ADDRESS")
        redis_port = os.getenv("ALC_REDIS_PORT")
        self.r = redis.StrictRedis(host=redis_host, port=redis_port, db=3)  # 3 = BOGUS
        self.current_abort = self.r.get(state.abort)
        self.current_busy = self.r.get(state.busy)
        self.alc_server = "http://127.0.0.1:5000"
        self.r.set(state.abort, state.abortFalse)
        self.r.set(state.busy, state.busyFalse)

    def testGet_listWorkers(self):
        self.r.set(state.abort, state.abortFalse)
        self.r.set(state.busy, state.busyFalse)
        msgCorrect = json.loads(
            '{"status":["bogus","bogus2","centerOnTip","samCamAuto","samCamManual","saveRasterGrid", "testSetup"]}'
        )
        url = self.alc_server + "/list"
        req = requests.get(url)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 200)
        self.assertEqual(msg, msgCorrect)

    def testAbort_bT_aT(self):
        # test /abort when: Busy=True, Abort = True
        self.r.set(state.abort, state.abortTrue)
        self.r.set(state.busy, state.busyTrue)
        msgCorrect = json.loads('{"status":"ABORT already in progress. Canceling request"}')
        url = self.alc_server + "/abort"
        req = requests.post(url)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 202)
        self.assertEqual(msg, msgCorrect)

    def testAbort_bF_aF(self):
        # test /abort when: Busy=False, Abort = False
        self.r.set(state.abort, state.abortFalse)
        self.r.set(state.busy, state.busyFalse)
        msgCorrect = json.loads('{"status":"ALC worker is not running. Nothing to ABORT."}')
        url = self.alc_server + "/abort"
        req = requests.post(url)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 202)
        self.assertEqual(msg, msgCorrect)

    def testAbort_bT_aF(self):
        # test /abort when: Busy=True, Abort = False
        self.r.set(state.abort, state.abortFalse)
        self.r.set(state.busy, state.busyTrue)
        msgCorrect = json.loads('{"status":"State changed to ABORT"}')
        url = self.alc_server + "/abort"
        req = requests.post(url)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 200)
        self.assertEqual(msg, msgCorrect)

    def testAbort_bF_aT(self):
        # test /abort when: Busy=False, Abort = True
        self.r.set(state.abort, state.abortTrue)
        self.r.set(state.busy, state.busyFalse)
        msgCorrect = json.loads('{"status":"ABORT already in progress. Canceling request"}')
        url = self.alc_server + "/abort"
        req = requests.post(url)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 202)
        self.assertEqual(msg, msgCorrect)

    def testBusy_bT(self):
        # test /busy when: Busy=True
        busyTrueMsg = json.loads('{"busy": true}')
        self.r.set(state.busy, state.busyTrue)
        url = self.alc_server + "/busy"
        req = requests.get(url)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 200)
        self.assertEqual(msg, busyTrueMsg)

    def testBusy_bF(self):
        # test /busy when: Busy=False
        busyFalseMsg = json.loads('{"busy": false}')
        self.r.set(state.busy, state.busyFalse)
        url = self.alc_server + "/busy"
        req = requests.get(url)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 200)
        self.assertEqual(msg, busyFalseMsg)

    def testWorkerBogus2(self):
        self.r.set(state.busy, state.busyFalse)
        self.r.set(state.abort, state.abortFalse)
        msgCorrect = json.loads(
            '{"busy":false,"data":"{\\"worker_out\\": null}","error":"None","name":"bogus2","result":"Success"}'
        )
        url = self.alc_server + "/run/bogus2"
        req = requests.post(url)
        urlBusy = self.alc_server + "/busy"
        urlStatus = self.alc_server + "/status"

        while True:  # Wait for job to finish
            req = requests.get(urlBusy)
            resp = json.loads(req.text)
            if not resp[state.busy]:
                break
            time.sleep(0.1)

        req = requests.get(urlStatus)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 200)
        self.assertEqual(msg, msgCorrect)

    def testWorkerCenterOnTip(self):
        self.r.set(state.busy, state.busyFalse)
        self.r.set(state.abort, state.abortFalse)
        msgCorrect = json.loads(
            '{"busy": false,\
                          "data": "{}", \
                          "error": "Failed to execute worker centerOnTip. Reason: \'centerOnTip\' object has no attribute \'samCam\'",\
                           "name": "centerOnTip","result": "Error"}'
        )

        lastError = "Failed to execute worker centerOnTip. Reason: 'centerOnTip' object has no attribute 'samCam'"
        url = self.alc_server + "/run/centerOnTip"

        req = requests.post(url)

        urlBusy = self.alc_server + "/busy"
        urlStatus = self.alc_server + "/status"

        while True:  # Wait for job to finish
            req = requests.get(urlBusy)
            resp = json.loads(req.text)
            # if resp[state.busy] == state.busyFalse:
            if not resp[state.busy]:
                break
            time.sleep(0.1)

        req = requests.get(urlStatus)
        code = req.status_code
        msg = json.loads(req.text)
        self.assertEqual(code, 200)
        self.assertEqual(msg, msgCorrect)

    def tearDown(self):
        # Revert to old setting
        self.r.set(state.abort, self.current_abort)
        self.r.set(state.busy, self.current_busy)


if __name__ == "__main__":
    unittest.main()
