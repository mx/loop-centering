import numpy as np
import sys
import unittest
import numpy as np
import cv2

sys.path.insert(0, "src/lib")

from loopImageProcessing import loopImageProcessing as lip


class TestImageProcessingX06DA(unittest.TestCase):
    def setUp(self):
        self.beamline = "X06DA"
        self.zoom = -500.0
        # Set up of a successful test case
        self.image = "tests/X06DA-zoom500-e025-g15.jpg"
        self.procImg = lip(self.image)
        self.procImg.findContour(beamline=self.beamline, zoom=self.zoom, hull=True)
        # Used as a positive control
        self.image2 = cv2.imread("tests/X06SA-raster.jpg")
        # Setup a blank image
        self.blank_img = np.zeros([100, 100, 3], dtype=np.uint8)
        self.blank_img.fill(255)
        self.proc_blank_img = lip(self.blank_img)

    def testExceptionLogging(self):
        # It is critical that this Exception message stays the same for SDU Executor
        with self.assertRaisesRegex(Exception, "Did not find loop contour."):
            self.proc_blank_img.findContour(beamline=self.beamline, zoom=self.zoom, hull=True)

    def testContourArea(self):
        area = self.procImg.contourArea()
        self.assertEqual(area, 84964.0, "Area of the contour different")

    def testFindTip(self):
        tip = self.procImg.findTip()
        np.testing.assert_array_equal(tip, np.array([512, 624]), "Tip position different")
        # self.assertEqual(tip, np.array([512, 624]), 'Tip position different')

    def testNoImage(self):
        with self.assertRaises(Exception):
            procImg = lip("bogus")

    def testNoZoomParams(self):
        with self.assertRaises(Exception):
            procImg = lip(self.image)
            procImg.findContour(zoom=10000000, beamline=self.beamline)

    def testNoContour(self):
        with self.assertRaises(Exception):
            procImg = lip(self.image)
            procImg.findTip()

    def testRasterHeatMap(self):
        procImg = lip(self.image2)
        procImg.rasterHeatMap(
            [
                [641, 403],
                [695, 403],
                [749, 403],
                [803, 403],
                [857, 403],
                [911, 403],
                [965, 403],
                [1019, 403],
                [1073, 403],
                [1127, 403],
                [1181, 403],
                [641, 433],
                [695, 433],
                [749, 433],
                [803, 433],
                [857, 433],
                [911, 433],
                [965, 433],
                [1019, 433],
                [1073, 433],
                [1127, 433],
                [1181, 433],
                [641, 463],
                [695, 463],
                [749, 463],
                [803, 463],
                [857, 463],
                [911, 463],
                [965, 463],
                [1019, 463],
                [1073, 463],
                [1127, 463],
                [1181, 463],
                [641, 493],
                [695, 493],
                [749, 493],
                [803, 493],
                [857, 493],
                [911, 493],
                [965, 493],
                [1019, 493],
                [1073, 493],
                [1127, 493],
                [1181, 493],
                [641, 523],
                [695, 523],
                [749, 523],
                [803, 523],
                [857, 523],
                [911, 523],
                [965, 523],
                [1019, 523],
                [1073, 523],
                [1127, 523],
                [1181, 523],
                [641, 553],
                [695, 553],
                [749, 553],
                [803, 553],
                [857, 553],
                [911, 553],
                [965, 553],
                [1019, 553],
                [1073, 553],
                [1127, 553],
                [1181, 553],
                [641, 583],
                [695, 583],
                [749, 583],
                [803, 583],
                [857, 583],
                [911, 583],
                [965, 583],
                [1019, 583],
                [1073, 583],
                [1127, 583],
                [1181, 583],
                [641, 613],
                [695, 613],
                [749, 613],
                [803, 613],
                [857, 613],
                [911, 613],
                [965, 613],
                [1019, 613],
                [1073, 613],
                [1127, 613],
                [1181, 613],
                [641, 643],
                [695, 643],
                [749, 643],
                [803, 643],
                [857, 643],
                [911, 643],
                [965, 643],
                [1019, 643],
                [1073, 643],
                [1127, 643],
                [1181, 643],
                [641, 673],
                [695, 673],
                [749, 673],
                [803, 673],
                [857, 673],
                [911, 673],
                [965, 673],
                [1019, 673],
                [1073, 673],
                [1127, 673],
                [1181, 673],
                [641, 703],
                [695, 703],
                [749, 703],
                [803, 703],
                [857, 703],
                [911, 703],
                [965, 703],
                [1019, 703],
                [1073, 703],
                [1127, 703],
                [1181, 703],
            ],
            np.array(
                [
                    [0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [12, 26, 12, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 10, 12, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                ]
            ),
            54,
            30,
            "bogus",
            26,
        )

        # filepath = os.path.join("tests", "raster_heatmap.jpg")
        cv2.imwrite("tests/raster_heatmap.jpg", img=self.image2)
        control_array = cv2.imread("tests/raster_heatmap_control.jpg")
        test_array = cv2.imread("tests/raster_heatmap.jpg")
        np.testing.assert_array_equal(control_array, test_array)


if __name__ == "__main__":
    unittest.main()
