#!/bin/bash

# Executing with "test" flag runs from git repository

if [ ! -d /scratch/$USER ] ; then
    mkdir /scratch/$USER;
fi
unset PYTHONPATH

# Get beamline name (in capitals)
BEAMLINE_XNAME=$1

export BEAMLINE_XNAME

if [ X$BEAMLINE_XNAME = X ] ;then
    echo "Beamline is undefined."
    exit 1
fi

bline_lowercase=$(echo $BEAMLINE_XNAME | tr A-Z a-z)

export PYTHONPATH=/sls/MX/applications/mxlibs3/all-beamlines/stable/:/sls/MX/applications/loop-centering/${bline_lowercase}/stable/lib

export EPICS_CA_MAX_ARRAY_BYTES=40000000 # required to run from root account

folder="/sls/MX/applications/loop-centering/${bline_lowercase}/stable"

if [ $# -eq 1 ] && [ X$1 = Xtest ] ;then
    folder="/sls/MX/applications/git/loop-centering/src"
fi

cd $folder

export ALC_PID_FILE=/scratch/alc_data/alc_${BEAMLINE_XNAME}.pid

if [ -f $ALC_PID_FILE ]; then rm $ALC_PID_FILE; fi

LOG_CONFIG=gunicorn_logging_${BEAMLINE_XNAME}.conf

/sls/MX/applications/conda_envs/alc_py39/bin/gunicorn app:app --worker-class gevent --workers 1 --timeout 60 --bind 0.0.0.0:1328  --log-config $LOG_CONFIG --pid=$ALC_PID_FILE
