#!/usr/bin/env python

import beamlineUtils as bu
import copy
import datetime
import globalStates as state
import hubclient
import logging
import numpy
import scipy.optimize
import time
import os
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

import centerOnTip
import findFlatFace
import findLoopBoundingBox
import findLoopThickness
from mx_preferences import get_config

beamline = os.environ.get("BEAMLINE_XNAME")
config = get_config(beamline)

logger = logging.getLogger("Worker-centerToFlat")


class centerToFlat(workerBaseClass.workerBaseClass):
    """
    Center on the biggest area and find bounding box for raster scan

    # ESCAPE STATE: SAMPLE ALIGNMENT
    """

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        setupSamCam=True,
        restoreSamCam=True,
        debug=True,
        saveImg=True,
        end_with_center_on_tip=True,
        *args,
        **kwargs,
    ):

        #
        self.setupSamCam = setupSamCam
        self.restoreSamCam = restoreSamCam
        self.end_with_center_on_tip = end_with_center_on_tip
        self.debug = debug
        self.saveImg = saveImg
        self.hub = hubclient.Hub(beamline.lower())
        self.smargon_installed = "smargon" == config["goniometer"].lower()
        self.single_axis_installed = "single-axis" == config["goniometer"].lower()

        # Define other workers we will use
        self.center_on_tip_job = centerOnTip.centerOnTip(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            saveImg=False,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
        )

        self.find_flat_face_job = findFlatFace.findFlatFace(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            saveImg=False,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
        )

        self.find_bounding_box_job = findLoopBoundingBox.findLoopBoundingBox(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
            saveImag=True,
        )

        self.find_loop_thickness_job = findLoopThickness.findLoopThickness(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
            saveImg=True,
        )

        super(centerToFlat, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        """Rotates omega along the angles given in omegaSteps, and use sin_fit to find biggest area.
        # ESCAPE STATE: SAMPLE ALIGNMENT
        """

        logger.info("Setting Sample Camera to fixed gain and exposure")

        if self.debug:
            start = time.time()

        if self.setupSamCam:
            self.samCam.setup()

        if self.debug:
            logger.info("Camera setup time: {}".format(time.time() - start))
            print("centerToFlat: Camera setup time: {}".format(time.time() - start))

        # Get image before centering on tip to see how well topcam performed
        # img = self.samCam.getImage(gray=False)
        # now = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        # procImg = lip(img)
        # procImg.saveImage('/scratch/alc_data/alc_images/images/beforeCenterOnTip', filename='{}.jpg'.format(now))

        if self.is_aborted():
            return {}

        # Find tip
        logger.info("centerToFlat: Executing CenterOnTip worker")
        if self.debug:
            start = time.time()
        tip_position = self.center_on_tip_job.worker()  # -> dict {beam_x: gmx, beam_y: gmy, beam_z, gmz}
        if self.debug:
            logger.info("centerToFlat: centerOnTip time: {}".format(time.time() - start))
            print("centerToFlat: centerOnTip time: {}".format(time.time() - start))

        if self.is_aborted():
            return {}

        # Zoom in
        logger.info("centerToFlat: zooming to flatFaceZoom")
        if self.debug:
            start = time.time()
        self.sampleMotor.zoomto(400)
        if self.debug:
            logger.info("centerToFlat: zooming time: {}".format(time.time() - start))
            print("centerToFlat: zooming time: {}".format(time.time() - start))

        if self.is_aborted():
            return {}

        # Center to flat
        logger.info("centerToFlat: finding flat face")
        if self.debug:
            start = time.time()
        self.find_flat_face_job.worker()
        if self.debug:
            logger.info("centerToFlat: flatFace time: {}".format(time.time() - start))
            print("centerToFlat: flatFace time: {}".format(time.time() - start))

        if self.is_aborted():
            return {}

        # Find loop thickness
        logger.info("Finding loop thickness")
        omega = self.omegaMotor.getOmega()
        self.omegaMotor.moveby(-90)
        thickness = self.find_loop_thickness_job.worker()
        self.omegaMotor.moveby(90)

        if self.is_aborted():
            return {}

        if self.end_with_center_on_tip:
            flat_omega = self.omegaMotor.getOmega()
            tip_position = self.center_on_tip_job.worker()  # -> dict {beam_x: gmx, beam_y: gmy, beam_z, gmz}
            self.omegaMotor.moveto(flat_omega)
            tip_position["omega"] = flat_omega

        logger.info("I am sleeping after 90 degree rotation")
        time.sleep(1.0)

        # IMPORTANT: Update tip_position to gonio-beamline coordinates for raster grid generation in SDU
        # IMPORTANT: This needs to be before final movement as a reference point for beam position in pixels
        if self.smargon_installed:
            tip_position["motor_x_at_tip"] = self.sampleMotor.smargon.bx  # tip position for grid scan
            tip_position["motor_y_at_tip"] = self.sampleMotor.smargon.by
            tip_position["motor_z_at_tip"] = self.sampleMotor.smargon.bz
        elif self.single_axis_installed:
            tip_position["motor_x_at_tip"] = self.sampleMotor.xrbv.get()
            tip_position["motor_y_at_tip"] = self.sampleMotor.yrbv.get()
            tip_position["motor_z_at_tip"] = self.sampleMotor.zrbv.get()

        logger.info(
            "tip_position is {}, {}, {}".format(
                tip_position["motor_x_at_tip"],
                tip_position["motor_y_at_tip"],
                tip_position["motor_z_at_tip"],
            )
        )

        # Save image before "finishing touches when the loop is moved 90um inwards
        img = self.samCam.getImage(gray=False)
        now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        procImg = lip(img)
        procImg.saveImage(
            "/scratch/alc_data/alc_images/images/flatFace",
            filename="{}.jpg".format(now),
        )

        # Save image after centerToFlat to see if pin base still icy
        pin_base_img = self.topCam.getImage()
        now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        pinProcImg = lip(pin_base_img)
        pinProcImg.saveImage(
            "/scratch/alc_data/alc_images/images/pinbase",
            filename=f"{now}.jpg",
        )

        logger.info("Moving the sample tip by {}".format(self.hub.getd("alc_final_movement_mm")))
        move = [float(self.hub.getd("alc_final_movement_mm")), 0.0]
        omega = self.omegaMotor.getOmega()
        moveXY = bu.xy2xyz(self.beamline, move, omega)

        if self.is_aborted():
            return {}

        self.sampleMotor.moveby_in_cryozone(moveXY[0], moveXY[1], moveXY[2])
        self.sampleMotor.wait()

        # Find bounding box and save it to image
        box = self.find_bounding_box_job.worker()
        logger.info("centerToFlat: Found bounding box: {}".format(box))

        if self.is_aborted():
            return {}

        # Don't go
        #        endZoom = self.hub.getd('alc_zoom_at_end')
        #        logger.info("Going to end zoom: {}".format(endZoom))
        #        self.sampleMotor.zoomto(endZoom)

        if self.restoreSamCam:
            self.samCam.restore()

        return dict(grid_corners=box, tip_position=tip_position, thickness=thickness)
