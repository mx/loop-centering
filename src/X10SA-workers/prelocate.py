#!/usr/bin/env python

import datetime
import logging
import time
import os
import hubclient
import numpy as np
import smargon
import backLight
import frontLight
import workerBaseClass
import workerRuntimeStates as workerRuntime
import beamlineUtils as bu
from loopImageProcessing import loopImageProcessing as lip
from mx_preferences import get_config

beamline = os.environ.get("BEAMLINE_XNAME")
config = get_config(beamline)

logger = logging.getLogger("Worker-prelocate")


class prelocate(workerBaseClass.workerBaseClass):
    """Prelocates beam using Top Cam

     ESCAPE STATE: SAMPLE EXCHANGE

    omegaStartAngle - important parameter, need to be chosen such that goniometer Y axis is aligned with topcam Y
    axis in such a way there is no need to transform move coordinates via rotaton matrix.
    tip_target = [x, y] # x,y position in top cam image where tip of the loop should be centered

     Args:
         debug:
         saveImg:
         *args:
         **kwargs:
    """

    def __init__(self, beamline, motors, redis_host, redis_port, redis_db, debug=False, saveImg=True, *args, **kwargs):
        self.hub = hubclient.Hub(beamline.lower())
        self.smargon = smargon.SmarGon()
        self.omegaStartAngle = self.hub.getd("alc_topcam_start_angle")
        self.omegaAngles = [self.omegaStartAngle, self.omegaStartAngle - 90]
        self.zoom = "topcam"
        self.backLight = backLight.backLight(beamline)
        self.frontLight = frontLight.frontLight(beamline)
        self.debug = debug
        self.saveImg = saveImg

        self.tip_target = [
            self.hub.getd("alc_topcam_beam_position_x"),
            self.hub.getd("alc_topcam_beam_position_y"),
        ]

        self.smargon_installed = "smargon" == config["goniometer"].lower()
        self.single_axis_installed = "single-axis" == config["goniometer"].lower()

        super(prelocate, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def tip2view(self, wait=True, swap_yz=False):
        """Move tip to location of beam position.

        Args:
            wait: if True, waits for sample motor to finish move before proceeding
            swap_yz: if True, does move [x, 0, y]. This is use when Omega is rotated 90 deg to Top Cam view axis

        """

        omega = self.omegaMotor.getOmega()

        logger.info(f"Prelocate tip for Omega={omega}")

        tip = np.array([0, 0])
        # The final tip location is calculated as average over n_samples.
        n_samples = 3
        for sample in range(n_samples):
            # Get image from camera
            img = self.topCam.getImage()
            # Process image
            procImg = lip(img)
            procImg.findContour(zoom=self.zoom, beamline=self.beamline, hull=True)
            # Find the tip of the loop on processed image
            sample_tip = procImg.findTip()
            tip += sample_tip
            logger.info(f"sample {sample} sample_tip location is {sample_tip}")
        tip = tip / n_samples
        tip.astype(int)

        logger.info(f"Tip found at x= {tip[0]}, y= {tip[1]} ")

        # Find how far is the tip from the beam in mm
        delta_mm = self.topCam.pixel_to_mm(tip - self.tip_target)
        logger.info(f"delta_mm is {delta_mm}")

        if self.smargon_installed:
            if not swap_yz:
                logger.info("Moving Sample Motor by {} {} {} @ Omega: {}".format(-delta_mm[0], -delta_mm[1], 0, omega))
                self.sampleMotor.moveby_in_cryozone(-delta_mm[0], -delta_mm[1], 0)
            else:
                logger.info(
                    "Moving Sample Motor by {} {} {} @ Omega".format(-delta_mm[0], 0, (-delta_mm[1] / 2), omega)
                )
                self.sampleMotor.moveby_in_cryozone(-delta_mm[0], 0, (-delta_mm[1] / 2))

        elif self.single_axis_installed:
            if not swap_yz:
                logger.info("Moving Sample Motor by {} {} {} @ Omega: {}".format(delta_mm[0], delta_mm[1], 0, omega))
                self.sampleMotor.moveby_in_cryozone(delta_mm[0], delta_mm[1], 0)
            else:
                logger.info("Moving Sample Motor by {} {} {} @ Omega".format(delta_mm[0], 0, -delta_mm[1], omega))
                self.sampleMotor.moveby_in_cryozone(delta_mm[0], 0, -delta_mm[1])

        if wait:
            self.sampleMotor.wait()

    def worker(self):
        """Centers loop on the Tip for given relative omega angles. Calls tip2view for each Omega"""

        logger.info("Prelocation: Setting Front Light to full power and Back Light off")
        self.backLight.setup()
        self.frontLight.setup()

        logger.info("Putting Aerotech to DIRECT mode")
        self.omegaMotor.set_direct()

        if self.smargon_installed:
            # Ensure that smargon chi and phi are at 0.0
            self.smargon.chi = 0.0
            self.smargon.phi = 0.0

        for i, omega in enumerate(self.omegaAngles):
            # Check if user aborted operation
            if self.stop():
                self.backLight.restore()
                self.frontLight.restore()
                self.result = workerRuntime.workerResultAbort
                logger.info("prelocate aborted")
                return {}

            self.omegaMotor.moveto(omega)
            # Don't release sample motor earlier as next step will be centerOnTip
            # - we need to make sure sample is in position
            waitForSampleMotor = True
            swap_yz = False
            if i == len(self.omegaAngles) - 1:
                swap_yz = True

            self.sampleMotor.wait()

            self.tip2view(wait=waitForSampleMotor, swap_yz=swap_yz)

        # Save image after prelocate has finished to see final lighting conditions
        if self.saveImg:
            now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            img = self.topCam.getImage()
            procImg = lip(img)
            procImg.saveImage(
                "/scratch/alc_data/alc_images/images/topcam",
                filename=f"{now}.jpg",
            )

        # Going from MSE -> SA moves gmy and gmz from escape_GmyMountPosition to escape_GmyAlignmentPosition.
        # Account for this offset
        gmy_offset = self.hub.getd("escape_GmyMeasurementPosition") - self.hub.getd("escape_GmyMountPosition")
        gmz_offset = self.hub.getd("escape_GmzMeasurementPosition") - self.hub.getd("escape_GmzMountPosition")

        if self.smargon_installed:
            start = self.smargon.readback_scs()
            self.smargon.shy = start["SHX"] - gmz_offset
            self.smargon.shx = start["SHY"] - gmy_offset
            logger.info(f"Moving SCS with gmy_offset {gmy_offset} and gmz_offset {gmz_offset}")
        elif self.single_axis_installed:
            omega = self.omegaMotor.getOmega()
            logger.info(
                "Prelocate - moving sample motors by escape_GmyMeasurementPosition - escape_GmyMountPosition offset: {} @ omega {}".format(
                    -gmy_offset, omega
                )
            )
            moveXY = bu.xy2xyz(self.beamline, [0, -gmy_offset], omega)
            logger.info("Moving by {}".format(moveXY))
            self.sampleMotor.moveby_in_cryozone(moveXY[0], moveXY[1], moveXY[2])

        self.sampleMotor.wait()

        # Move Omega motor back to 0
        self.omegaMotor.moveto(0)
        self.sampleMotor.wait()

        logger.info("Prelocate: Switching Off Front Light")
        self.frontLight.restore()

        logger.info(f"Prelocate: Omega back to {self.omegaMotor.getOmega()}")

        return {}
