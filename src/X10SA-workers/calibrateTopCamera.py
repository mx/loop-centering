#!/usr/bin/env python

import backLight
import beamlineUtils as bu
import datetime
import frontLight
import globalStates as state
import hubclient
import logging
import time
import os
import smargon
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip
from mx_preferences import get_config

beamline = os.environ.get("BEAMLINE_XNAME")
config = get_config(beamline)


logger = logging.getLogger("Worker-calibrateTopCamera")


class calibrateTopCamera(workerBaseClass.workerBaseClass):
    """Updates the beam position on top camera so sample is in focus after mse2sa"""

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        setupLighting=True,
        debug=False,
        saveImg=False,
        *args,
        **kwargs,
    ):
        self.setupLighting = setupLighting
        self.backLight = backLight.backLight(beamline.lower())
        self.frontLight = frontLight.frontLight(beamline.lower())
        self.debug = debug
        self.saveImg = saveImg
        self.hub = hubclient.Hub(beamline.lower())
        self.smargon_installed = "smargon" == config["goniometer"].lower()
        self.single_axis_installed = "single-axis" == config["goniometer"].lower()
        self.omegaStartAngle = self.hub.getd("alc_topcam_start_angle")
        self.smargon = smargon.SmarGon()


        # x,y position in top cam image where tip of the loop should be centered
        self.tip_target = [
            self.hub.getd("alc_topcam_beam_position_x"),
            self.hub.getd("alc_topcam_beam_position_y"),
        ]

        super(calibrateTopCamera, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        """Updates the beam position on top camera so sample is in focus after mse2sa"""

        logger.info("calibrateTopCamera: Setting Back Light to full power and Front Light to 0.2V")
        self.backLight.setup()
        self.frontLight.setup()

        logger.info("Putting Aerotech to DIRECT mode")
        self.omegaMotor.set_direct()

        omega = self.omegaMotor.moveto(self.omegaStartAngle)
        logger.info("Successfully moved to alc omega start angle: {}".format(omega))

        self.sampleMotor.loop_escape_transition("sa2mse")

        time.sleep(3.0)

        if self.smargon_installed:
            logger.info("Moving smargon to chi 0 and phi 0")
            self.smargon.chi = 0
            self.smargon.phi = 0
            time.sleep(1)


        img = self.topCam.getImage()
        procImg = lip(img)
        procImg.findContour(zoom="topcam", beamline=self.beamline, hull=True)
        tip = procImg.findTip()
        logger.info("Old determined Tip location {}".format(self.tip_target))
        logger.info("New determined Tip location {}".format(tip))

        self.hub.put("alc_topcam_beam_position_x", tip[0])
        self.hub.put("alc_topcam_beam_position_y", tip[1])

        if self.saveImg:
            now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            img = self.topCam.getImage()
            procImg = lip(img)
            procImg.saveImage(
                "/scratch/alc_data/alc_images/images/topcam",
                filename="{}.jpg".format(now),
            )

        self.sampleMotor.loop_escape_transition("mse2sa")

        self.backLight.restore()
        self.frontLight.restore()

        return {}
