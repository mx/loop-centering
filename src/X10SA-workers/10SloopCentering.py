#!/usr/bin/env python

# Script to capture vido from Sample camera on given beamline.
# To stop capturing press 'q' in the view window_len
# The output is saved to output-{beamline}.avi
# Uses epicsAD module from Xiaoxiang ALC

import sys

sys.path.insert(0, "../lib")
import beamlineMotors
import epicsAD

import cv2

from loopImageProcessing import loopImageProcessing as lip


def toRGB(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


def rotate(img, angle):
    shape = img.shape
    cols = shape[0]
    rows = shape[1]
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
    newImg = cv2.warpAffine(img, M, (cols, rows))

    return newImg


def transformX06DA(img):
    # on 6D image needs to be rotated
    img = rotate(img, -90)
    # and mirrored
    img = cv2.flip(img, 1)
    # and put it to RGB
    # img = toRGB(img)
    return img


def transformX06SA(img):
    # flip image horizontal and than vertical to get the same view as DA+GUI
    img = cv2.flip(img, -1)
    img = cv2.flip(img, 0)
    return img


# Setup access to area detector and beamline motors
cameras = {
    "X06DA": "X06DA-SAMCAM:",
    "X06SA": "X06SA-D3CAM:",
    "X10SA": "X10SA-SAMCAM:",
}

beamline = "X10SA"
ad = epicsAD.epicsAD(cameras[beamline])
sampleMotor = eval("beamlineMotors.SampleMotor%s()" % beamline)
omegaMotor = beamlineMotors.OmegaMotor(beamline)

# Find where is the image center for given camera
imgCenter = ad.getImageCenter()

#### Start at zoom 2x ####
zoom = 2
sampleMotor.zoomto(zoom)

# Move the sample out of the shadow
sampleMotor.moveby(0, 0, -0.5)
sampleMotor.wait()

# Get image and find the tip of the sample
img = ad.getImage(gray=False)
if beamline == "X06DA":
    img = transformX06DA(img)
img = toRGB(img)

procImg = lip(img, zoom)
procImg.showTip()
tip = procImg.findTip()

delta = (imgCenter[0] - tip[0], imgCenter[1] - tip[1])
deltaMM = sampleMotor.pixel_to_mm(delta)

# # Center on the tip of the loop
# sampleMotor.moveby(dx,dy,dz)


# #### Set motor to zoom 5 #####

zoom = 5
sampleMotor.zoomto(zoom)

for omega in [0, 90]:
    omegaMotor.moveby(omega)
    img = ad.getImage(gray=False)
    if beamline == "X06DA":
        img = transformX06DA(img)
    if beamline == "X06SA":
        img = transformX06SA(img)
    img = toRGB(img)

    procImg = lip(img, zoom)
    procImg.showTip()
    tip = procImg.findTip()

    delta = (imgCenter[0] - tip[0], imgCenter[1] - tip[1])
    deltaMM = sampleMotor.pixel_to_mm(delta)
    # Center on the tip of the sample
    # sampleMotor.moveby(dx,dy,dz)

# # Analyze the contour and find center of the mass

# # Start rotation by 20 deg analyzign the contour


# if cv2.waitKey(1) & 0xFF == ord('s'):
# 	screnshotName = "%s-screenshots/frame-%i.jpg"%(beamline, frameNo)
# 	print screnshotName
# 	cv2.imwrite(screnshotName, img)
# if cv2.waitKey(1) & 0xFF == ord('q'):
# 	break
# frameNo += 1

# # out.release()
# cv2.destroyAllWindows()
