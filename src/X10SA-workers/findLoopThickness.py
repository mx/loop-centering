#!/usr/bin/env python

import beamlineUtils as bu
import globalStates as state
import hubclient
import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

logger = logging.getLogger("Worker-findLoopThickness")


class findLoopThickness(workerBaseClass.workerBaseClass):
    """Find loop thickness at current Omega angle"""

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        setupSamCam=True,
        restoreSamCam=True,
        debug=False,
        saveImg=True,
        apply_finishing_touches=False,
        *args,
        **kwargs,
    ):

        self.setupSamCam = setupSamCam
        self.restoreSamCam = restoreSamCam
        self.debug = debug
        self.saveImg = saveImg
        self.hub = hubclient.Hub(beamline.lower())
        self.apply_finishing_touches = apply_finishing_touches

        super(findLoopThickness, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def get_thickness(self, debug=False):
        """
        Finds loop thicknessa at current Omega angle
        :param wait:
        :param debug:
        :return:
        """

        zoom = self.sampleMotor.currentZoom()

        # Get image from camera
        img = self.samCam.getImage(gray=False)
        # Process image
        procImg = lip(img)
        procImg.findContour(zoom=zoom, beamline=self.beamline, hull=True)

        extremes = procImg.findExtremes()
        thickness_px = abs(extremes["bottom"][1] - extremes["top"][1])
        thickness = self.sampleMotor.pixel_to_mm(thickness_px) * 1000.0  # px -> mm -> um

        if self.saveImg:
            procImg.showExtremes(show_img=False)
            procImg.saveImage(directory="/scratch/alc_data/alc_images/images/thickness")

        return thickness

    def finishing_touches(self, save_img=False, save_image_dir=None):
        pass

    def worker(self):
        """ "
        # ESCAPE STATE: SAMPLE ALIGNMENT
        """
        if self.setupSamCam:
            logger.info("Setting Sample Camera to fixed gain and exposure")
            self.samCam.setup()

        thickness = self.get_thickness(debug=self.debug)

        if self.restoreSamCam:
            logger.info("Restoring sample camera to automatic gain and exposure")
            self.samCam.restore()

        # Finishing touches
        if self.apply_finishing_touches:
            self.finishing_touches()

        return dict(thickness=thickness, unit="um")
