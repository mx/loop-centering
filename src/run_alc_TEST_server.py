#!/usr/bin/env python
import sys

sys.path.insert(0, "src")
sys.path.insert(0, "src/lib")
sys.path.insert(0, "src/etc")
sys.path.insert(0, "src/app")
sys.path.insert(0, "../../mxlibs3/src")

from app import app

app.run()
