""" Redis location can be set with environment variables, will default to production values """

import os

beamline = os.getenv("BEAMLINE_XNAME", None)

if beamline:
    redis_host = f"{beamline.lower()}-cons-705.psi.ch"
    redis_port = 6379
    redis_db = {"X06DA": 1, "X06SA": 1, "X10SA": 1, "BOGUS": 1}
else:
    redis_host = os.getenv("ALC_REDIS_ADDRESS", "mx-db1.psi.ch")
    redis_port = int(os.getenv("ALC_REDIS_PORT", "6379"))
    redis_db = {"X06DA": 0, "X06SA": 1, "X10SA": 2, "BOGUS": 3}
