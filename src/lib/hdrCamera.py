import cv2
import numpy as np
import requests


class hdrCamera(object):
    """
    Class to operate HDR Camera and take images of the goniometer when sample mounting fails in SDU
    """

    def __init__(self, beamline):
        self.beamline = beamline.upper()

        self.cameras = {
            "X06SA": "http://axis-accc8ea5e463.psi.ch/jpg/3/image.jpg",
            "X06DA": "http://axis-accc8ed2972e.psi.ch/jpg/2/image.jpg",
            "X10SA": "http://x10sa-axis-3/jpg/3/image.jpg",
        }

        if self.beamline not in self.cameras.keys():
            raise Exception(f"TopCam: Beamline needs to be one of: {list(self.cameras.keys())}")

    def getImage(self):
        """
        Gets the image from HDR Camera @ self.beamline
        """
        r = requests.get(self.cameras[self.beamline])
        img = np.asarray(bytearray(r.content), dtype="uint8")
        img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        img = getattr(self, "processImage%s" % self.beamline)(img)
        return img

    def saveImage(self, img, filename):
        """
        Saves image to file
        :param img: numpy array
        :param filename: file name to save. Extension .jpg
        :return:
        """
        cv2.imwrite(filename, img)

    def processImageX06DA(self, img):
        # Specific operations to process the image from X06DA top camera
        return img

    def processImageX06SA(self, img):
        # Specific operations to process the image from X06SA top camera
        return img

    def processImageX10SA(self, img):
        # Specific operations to process the image from X10SA top camera
        return img
