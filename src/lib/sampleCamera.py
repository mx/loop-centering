import cv2
import hubclient
import time

import backLight
import epicsAD
import frontLight


class sampleCamera(object):
    def __init__(self, beamline):
        self.beamline = beamline.upper()
        self.hub = hubclient.Hub(beamline.lower())

        self.cameras = {
            "X06DA": "X06DA-SAMCAM:",
            "X06SA": "X06SA-D3CAM:",
            "X10SA": "X10SA-SAMCAM:",
        }

        self.camera_setup = {
            "X06DA": {
                "default": {"exposure": 0.025, "gain": 6},
                "alc": {
                    "exposure": self.hub.getd("alc_samcam_exposure"),
                    "gain": self.hub.getd("alc_samcam_gain"),
                },
            },  # exposure 0.025 gain 15
            "X06SA": {
                "default": {"exposure": 0.016, "gain": 0},
                "alc": {
                    "exposure": self.hub.getd("alc_samcam_exposure"),
                    "gain": self.hub.getd("alc_samcam_gain"),
                },
            },  # exposure was 0.025 gain 10
            "X10SA": {
                "default": {"exposure": 0.002, "gain": 0},
                "alc": {
                    "exposure": self.hub.getd("alc_samcam_exposure"),
                    "gain": self.hub.getd("alc_samcam_gain"),
                },
            },
        }

        if self.beamline not in self.cameras.keys():
            raise Exception("Beamline needs to be one of: %s" % str(self.cameras.keys()))
        self.ad = epicsAD.epicsAD(self.cameras[self.beamline])
        self.fl = frontLight.frontLight(self.beamline)
        self.bl = backLight.backLight(self.beamline)

    def __toRGB(self, img):
        return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    def __rotate(self, img, angle):
        shape = img.shape
        cols = shape[0]
        rows = shape[1]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
        newImg = cv2.warpAffine(img, M, (cols, rows))
        return newImg

    def setup(self):
        # Set microscope to RGB mode (2) if not already in RGB mode
        if self.ad.color.get() != 2:
            self.ad.color.put(2)
        self.bl.restore()  # bring back light to max illumination
        self.fl.restore()  # turn front light off
        gainVal = self.camera_setup[self.beamline]["alc"]["gain"]
        expoVal = self.camera_setup[self.beamline]["alc"]["exposure"]
        self.ad.setup(gainVal=gainVal, expoVal=expoVal)
        time.sleep(1.0)

    def restore(self):
        gainVal = self.camera_setup[self.beamline]["default"]["gain"]
        expoVal = self.camera_setup[self.beamline]["default"]["exposure"]
        self.ad.restore(gainVal=gainVal, expoVal=expoVal)
        time.sleep(1.0)

    def getImage(self, gray=False):
        """Gets the image from Sample Camera @ self.beamline"""
        img = self.ad.getImage(gray=gray)
        # Process the image for given beamline
        img = getattr(self, "processImage%s" % self.beamline)(img)
        return img

    def saveImage(self, img, filename):
        """
        Saves image to file
        :param img: numpy array
        :param filename: file name to save. Extension .jpg
        :return:
        """
        cv2.imwrite(filename, img)

    def processImageX06DA(self, img):
        # Specific operations to process the image from X06DA sample camera
        img = self.__rotate(img, -90)
        # and mirrored
        img = cv2.flip(img, 1)
        img = self.__toRGB(img)
        return img

    def processImageX06SA(self, img):
        # Specific operations to process the image from X06SA sample camera
        img = cv2.flip(img, -1)
        img = cv2.flip(img, 0)
        img = self.__toRGB(img)
        return img

    def processImageX10SA(self, img):
        # Specific operations to process the image from X10SA sample camera
        img = self.__toRGB(img)
        return img


if __name__ == "__main__":
    samcam = sampleCamera("X06SA")
    samcam.setup()
    time.sleep(2)
    samcam.restore()
    # fl = frontLight('X10SA')
