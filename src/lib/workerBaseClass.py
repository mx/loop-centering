#!/usr/bin/env python

import functools
import json
import logging
import redis
import sys
import threading
import time

import beamlineUtils as bu
import globalStates as state
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("AlcWorker")


class workerBaseClass(threading.Thread):
    def __init__(self, beamline, motors, redis_host, redis_port, redis_db, *args, **kwargs):
        super(workerBaseClass, self).__init__(name="ALCWorkerThread")
        self.daemon = True  # True forces thread to exit when parent is not running

        try:
            self.beamline = bu.validateBeamline(beamline)
        except Exception as e:
            msg = "Cannot find beamline name: {}".format(e)
            logger.info(msg)
            raise Exception(msg)

        if beamline != "BOGUS":
            self.samCam = motors["samCam"]
            self.sampleMotor = motors["sampleMotor"]
            self.omegaMotor = motors["omegaMotor"]
            self.frontLight = motors["frontLight"]
            #            self.backLight   = motors['backLight']
            self.topCam = motors["topCam"]

        self.args = args
        self.kwargs = kwargs

        # Access to REDIS server
        self._r = redis.StrictRedis(host=redis_host, port=redis_port, db=redis_db)

        # Set worker Runtime varaibles
        self.myName = type(self).__name__  # This is the name of the class
        self.result = workerRuntime.workerResultNone
        self.error = workerRuntime.workerErrorMsgNone
        self.worker_return_data = workerRuntime.workerReturnDataNone

        # Save the current worker name in Redis
        self._r.set(state.currentWorker, self.myName)

    # Wrapper to wrap the worker function in try/except/finally block.
    # Executes setup and restore before and after worker execution
    # Sets state Busy=False after worker is executed
    def worker_logic(worker_func):
        @functools.wraps(worker_func)
        def wrapper(self, *args, **kwargs):
            try:
                start = time.time()
                worker_out = worker_func(self, *args, **kwargs)  # Run the worker thread
                # If worker didn't Failed, make sure it is Success
                if self.result == workerRuntime.workerResultNone:
                    self.result = workerRuntime.workerResultSuccess
                self.worker_return_data = json.dumps(dict(worker_out=worker_out))
                logger.info(
                    "Worker {} finished in {:.1f} sec. Result: {}.".format(
                        self.myName, time.time() - start, self.result
                    )
                )
            except Exception as e:
                #                self.samCam.restore()  # If the job finished, make sure that sample camera is restored to Auto
                self.result = workerRuntime.workerResultError
                self.worker_return_data = workerRuntime.workerReturnDataNone
                msg = "Failed to execute worker {}. Reason: {}".format(self.myName, e)
                self.error = msg
                logger.info(msg)
            finally:
                self.__restoreState()

        return wrapper

    def stop(self):
        return self._r.get(state.abort) == state.abortTrue

    def is_aborted(self):
        """Gracefully abort job"""
        if self.stop():
            self.samCam.restore()
            self.result = workerRuntime.workerResultAbort
            logger.info("centering worker aborted")
            return True
        return False

    def __restoreState(self):
        # Set Busy to False to indicate that process is finished.
        # THIS IS THE ONLY PLACE WHERE Busy is changed back to False
        # Since only one thread at the time is allowed to run,
        # it is safe to change back without REDIS WATCH
        #
        with self._r.pipeline() as pipe:
            pipe.multi()
            # Restore Global states
            pipe.set(state.busy, state.busyFalse)
            pipe.set(state.abort, state.abortFalse)

            # Indicate worker Runtime information as lastWorker* in REDIS
            pipe.set(workerRuntime.workerName, self.myName)
            pipe.set(workerRuntime.workerResult, self.result)
            pipe.set(workerRuntime.workerErrorMsg, self.error)
            pipe.set(workerRuntime.workerReturnData, self.worker_return_data)
            pipe.set(state.currentWorker, state.currentWorkerNone)
            pipe.execute()

    def worker(self):
        """Implement some work to do when subclassed"""
        # Example of control
        if self.stop():  # check self.stop() periodically to see if abort was requested
            self.result = workerRuntime.workerResultAbort
            return

        something_went_not_as_planned = False
        if something_went_not_as_planned:
            self.result = workerRuntime.workerResultFail
        return []  # Return something if needed or empty list

    @worker_logic
    def wrapped_worker(self):
        # Wrap the worker. This is executed by the the run function
        # NOTE: we use it here instead of worker directly, so we don't have to use the decorator in the children classes
        worker_result = self.worker()
        return worker_result

    def run(self):
        # Run the worker thread
        logger.info("Starting worker thread")
        self.wrapped_worker()
        logger.info("Worker thread finished")
