import epics
import numpy as np


class epicsAD(object):
    def __init__(self, prefix, cam="cam1:", image="image1:"):
        self.img = None
        self.monitored = False

        self.color = epics.PV(prefix + cam + "ColorMode")
        self.gain_mode = epics.PV(prefix + cam + "GainMode")  # 0 - manual, 2 - auto
        self.expo_mode = epics.PV(prefix + cam + "ExposureMode")  # 0 - manual, 2 - auto
        self.gain = epics.PV(prefix + cam + "Gain")
        self.expo = epics.PV(prefix + cam + "AcquireTime")

        self.ndim = epics.PV(prefix + image + "NDimensions_RBV")
        self.dim0 = epics.PV(prefix + image + "ArraySize0_RBV")
        self.dim1 = epics.PV(prefix + image + "ArraySize1_RBV")
        self.dim2 = epics.PV(prefix + image + "ArraySize2_RBV")
        self.uid = epics.PV(prefix + image + "UniqueId_RBV")
        self.data = epics.PV(prefix + image + "ArrayData")

        try:
            epics.ca.pend_io()
        except:
            pass

    def monitorCB(self, epicsArgs, userArgs):
        self.img = epicsArgs["pv_value"]
        self.img.dtype = np.uint8
        self.img.shape = self.shape
        self.monitored = True

    def setMonitor(self):
        ndim = self.ndim.get()
        dim0 = self.dim0.get()
        dim1 = self.dim1.get()
        dim2 = self.dim2.get()
        if ndim == 3:
            size = dim0 * dim1 * dim2
            self.shape = (dim2, dim1, dim0)
        else:
            size = dim0 * dim1
            self.shape = (dim1, dim0)
        self.data.add_masked_array_event(None, size, None, self.monitorCB)

    def clearMonitor(self):
        self.data.clear_event()
        self.monitored = False

    def getShape(self):
        ndim = self.ndim.get()
        dim0 = self.dim0.get()
        dim1 = self.dim1.get()
        dim2 = self.dim2.get()

        if ndim == 3:
            shape = (dim2, dim1, dim0)
        else:
            shape = (dim1, dim0)
        return shape

    def getImageCenter(self):
        shape = self.getShape()
        return (shape[0] / 2, shape[1] / 2)

    def getImage(self, gray=True):
        if self.monitored:
            return self.img

        ndim = self.ndim.get()
        dim0 = self.dim0.get()
        dim1 = self.dim1.get()
        dim2 = self.dim2.get()
        if ndim == 3:
            size = dim0 * dim1 * dim2
        else:
            size = dim0 * dim1
        data = self.data.get(count=size)
        data.dtype = np.uint8

        if ndim == 3:
            data.shape = (dim2, dim1, dim0)
            # convert to grayscale
            if gray:
                data = data[:, :, 0] * 299.0 / 1000.0 + data[:, :, 1] * 587.0 / 1000.0 + data[:, :, 2] * 114.0 / 1000.0
        else:
            data.shape = (dim1, dim0)

        return data

    def setup(self, gainVal=15, expoVal=0.025):
        # self.color.put(0) # black and white
        self.gain_mode.put(0)  # manual gain control
        self.gain.put(gainVal)
        self.expo_mode.put(0)  # manual exposure control
        self.expo.put(expoVal)  # 20 hz acquisition

    def restore(self, gainVal=5, expoVal=0.025):
        # Put this values, so the camera faster resores proper values in Auto mode
        self.gain.put(gainVal)
        self.expo.put(expoVal)
        self.gain_mode.put(2)  # automatic gain control
        self.expo_mode.put(2)  # automatic exposure control


if __name__ == "__main__":
    ad = epicsAD("X06SA-D3CAM:")
    # ad = epicsAD('13SIM1:')
    # ad.setup()
    im = ad.getImage(gray=False)
