import json

workerName = "lastWorkerName"  # this is Redis key name to store worker name
workerNameNone = "None"

workerErrorMsg = "lastWorkerError"  # this is Redis key name to store worker error messages
workerErrorMsgNone = "None"

workerResult = "lastWorkerResult"  # this is Redis key name to store worker Result
workerResultNone = "None"
workerResultSuccess = "Success"  # Indicates everything went OK
workerResultFail = (
    "Failed"  # Indicates everything went OK, no Errors, but Centering itself failed (for instance, loop not detected)
)
workerResultAbort = "Aborted"
workerResultError = "Error"

workerReturnData = "lastWorkerReturnData"  # this is Redis key name to store worker Result
workerReturnDataNone = json.dumps({})  # the result of worker processing
