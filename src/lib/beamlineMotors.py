#!/usr/bin/env python
# epics waveform get as numpy array
import os
import epics
import logging
import time
import numpy as np

import hubclient
import smargon
from mx_preferences import get_config

beamline = os.environ.get("BEAMLINE_XNAME")
config = get_config(beamline)

logging.basicConfig(format="%(asctime)s - %(levelname)s : %(message)s")
logger = logging.getLogger("ALC")

cameras = {
    "X06DA": "X06DA-SAMCAM:",
    "X06SA": "X06SA-D3CAM:",
    "X10SA": "X10SA-SAMCAM:",
}


class SampleMotorX10SA(object):
    def __init__(self):
        self.smargon_installed = "smargon" == config["goniometer"].lower()
        self.single_axis_installed = "single-axis" == config["goniometer"].lower()

        if self.smargon_installed:
            # Smargon co-ordinates for ALC jobs - x = SHZ, y = SHY, z = SHX
            # Beamline co-ordinates for gridscans - x = BX, y = BY, z = BZ
            self.smargon = smargon.SmarGon()

        elif self.single_axis_installed:
            # relative movement
            self.dx = epics.PV("X10SA-ES-DF1:GMX-INCP")
            self.dy = epics.PV("X10SA-ES-DFS:TRY1.RLV")
            self.dz = epics.PV("X10SA-ES-DFS:TRZ1.RLV")
            # absolute movements
            self.x = epics.PV("X10SA-ES-DF1:GMX-VAL")
            self.y = epics.PV("X10SA-ES-DFS:TRY1")
            self.z = epics.PV("X10SA-ES-DFS:TRZ1")
            # actual position
            self.xrbv = epics.PV("X10SA-ES-DF1:GMX-RBV")
            self.yrbv = epics.PV("X10SA-ES-DFS:TRY1.RBV")
            self.zrbv = epics.PV("X10SA-ES-DFS:TRZ1.RBV")
            # movement finishes?
            self.xdone = epics.PV("X10SA-ES-DF1:GMX-DONE")
            self.ydone = epics.PV("X10SA-ES-DFS:TRY1.DMOV")
            self.zdone = epics.PV("X10SA-ES-DFS:TRZ1.DMOV")

        # Relative movement of aerotech for top camera calibration
        self.dgmx = epics.PV("X10SA-ES-DF1:GMX-INCP")
        self.dgmy = epics.PV("X10SA-ES-DF1:GMY-INCP")
        self.dgmz = epics.PV("X10SA-ES-DF1:GMZ-INCP")

        # Collimator
        self.colli = epics.PV("X10SA-ES-MIC:TRY2.VAL")

        # lamp
        self.lamp_get = epics.PV("X10SA-ES-KOI:GET-POS")
        self.lamp_set = epics.PV("X10SA-ES-KOI:SET-POS")

        # front light
        self.front_light = epics.PV("X10SA-ES-FL1:SET-LIGHT")

        # cryo
        self.cryo = epics.PV("X10SA-ES-CJ:TRY1.VAL")

        self.gui_zoom_levels = {
            1.0: 1,
            1.7: 200.0,
            2.7: 400.0,
            4.5: 600.0,
            5.8: 700.0,
            7.5: 800.0,
            9.7: 900.0,
            11.0: 950,
        }

        # zoom motor
        self.zoom_set = epics.PV("X10SA-SAMCAM:ZOOM-VAL")
        self.zoom_get = epics.PV("X10SA-SAMCAM:ZOOM-RBV")
        self.current_zoom_level = self.zoom_get.get()  # current zoom motor position

        self.hub = hubclient.Hub(beamline.lower())

    def setup(self):
        # move lamp down to get a dark background
        self.lamp_pos = self.lamp_get.get(as_string=True)
        self.lamp_set.put("Park")
        self.front_light.put(5)  # what value to put here? LOPR = -10, HOPR = 10, PREC = 2

    def restore(self):
        # restore lamp position
        self.lamp_set.put(self._lamp_pos)

    def moveto(self, x, y, z):
        if self.smargon_installed:
            self.smargon.shz = x
            self.smargon.shy = y
            self.smargon.shx = z
        elif self.single_axis_installed:
            self.x.put(x, wait=True)
            self.y.put(y, wait=True)
            self.z.put(z, wait=True)

    def movetoraster(self, x, y, z):
        if self.smargon_installed:
            # Moves the Smargon to first cell of raster scan
            target = (x, y, z)
            self.smargon.xyz(target)
        else:
            logger.info("movetoraster method not implemented for single-axis")

    def moveby(self, dx, dy, dz):
        if self.smargon_installed:
            # Moves smargon using SCS coordinates
            start = self.smargon.readback_scs()
            x = dx + start["SHZ"]
            y = dz + start["SHY"]
            z = dy + start["SHX"]
            self.moveto(x, y, z)
        elif self.single_axis_installed:
            # x = self.xrbv.get() + dx
            # avoid movement beyond croygenic zone
            if abs(dx) > 1:
                return
            self.dx.put(dx)
            self.dy.put(dy)
            self.dz.put(dz)
            self.wait()

    def movebyraster(self, dx, dy, dz):
        if self.smargon_installed:
            # Moves the smargon to first cell of raster scan
            start = self.smargon.readback_bcs()
            x = dx + start["BX"]
            y = dy + start["BY"]
            z = dz + start["BZ"]
            target = (x, y, z)
            self.smargon.xyz(target)
        else:
            logger.info("movebyraster method not implemented for single-axis")

    def moveby_in_cryozone(self, dx, dy, dz):
        if self.smargon_installed:
            # Moves the Smargon within cryozone limits. x-cooridnate is always the same, y,z- depends on Omega
            # Limits in um
            xlimit = [15.5, 21.5]  # smargon limits are 10 - 25
            ylimit = [-2.5, 2.5]
            zlimit = ylimit

            start = self.smargon.readback_scs()
            x = start["SHZ"]
            y = start["SHY"]
            z = start["SHX"]

            if (
                (xlimit[0] < dx + x < xlimit[1])
                and (ylimit[0] < dy + y < ylimit[1])
                and (zlimit[0] < dz + z < zlimit[1])
            ):
                self.moveby(dx, dy, dz)

        elif self.single_axis_installed:
            # Moves the smaracts within cryozone limits (in um)
            # x-cooridnate is always the same, y,z- depends on Omega
            xlimit = [-20, -16]
            ylimit = [-2, 2]
            zlimit = [-2, 2]

            x = self.xrbv.get()
            y = self.yrbv.get()
            z = self.zrbv.get()

            if (
                (xlimit[0] < dx + x < xlimit[1])
                and (ylimit[0] < dy + y < ylimit[1])
                and (zlimit[0] < dz + z < zlimit[1])
            ):
                self.moveby(dx, dy, dz)

    def wait(self):
        if self.smargon_installed:
            time.sleep(0.5)  # wait for a status to change?
        elif self.single_axis_installed:
            time.sleep(0.2)  # wait for final issued motor movement to finish?
            while self.xdone.get() == 0 or self.ydone.get() == 0 or self.zdone.get() == 0:
                time.sleep(0.1)

    # use camera translation factors for calculation of pixel_to_mm conversion factor
    def pixel_to_mm(self, pixel):
        pixel = np.array(pixel)
        logger.info("X10SA pixel is: {}".format(pixel))
        zoom = self.currentZoom()
        logger.info("X10SA zoom is: {}".format(zoom))
        # Get the parameters of the polynomial equation from the HUB
        a = float(self.hub.find("camera_translationFactorA")["camera_translationFactorA"])
        b = float(self.hub.find("camera_translationFactorB")["camera_translationFactorB"])
        conversion = np.exp(b + (a * zoom))
        logger.info("X10SA conversion is: {}".format(conversion))
        logger.info("X10SA pixel_to_mm is: {}".format(pixel / conversion))
        return pixel / conversion

    def zoomto(self, zoom_level):
        """zoomto using gui zoom levels"""
        try:
            motor_target = self.gui_zoom_levels[zoom_level]
        except KeyError:
            raise Exception("Available zoom levels: ", self.gui_zoom_levels.keys())

        self.current_zoom_level = motor_target
        self.zoom_set.put(motor_target, wait=True)

    def zoomto(self, motor_target):
        """zoomto using motor zoom levels"""

        self.current_zoom_level = motor_target
        self.zoom_set.put(motor_target, wait=True)

    def guiZoomToMotor(self, guiZoom):
        try:
            return self.gui_zoom_levels[guiZoom]
        except KeyError:
            raise Exception("Available zoom levels: ", self.gui_zoom_levels.keys())

    def currentZoom(self):
        # default timeout = 1second increased to compensate for network issues/slowness
        self.current_zoom_level = self.zoom_get.get(timeout=2)
        return self.current_zoom_level

    def num_zoom_levels(self):
        return len(self.zoom_level)

    def currentAperture(self):
        current_aperture = self.aperture.get()
        if current_aperture in self.aperture_sizes:
            return self.aperture_sizes[current_aperture]

    def aerotech_offsets(self):
        gmy_offset = self.hub.getd("escape_GmyMeasurementPosition")
        gmz_offset = self.hub.getd("escape_GmzMeasurementPosition")
        return gmy_offset, gmz_offset

    def cryo_positions(self):
        cryo_sa = self.hub.get("escape_CryojetMeasurementPosition")
        cryo_mse = self.hub.get("escape_CryojetParkPosition")
        return cryo_sa, cryo_mse

    def loop_escape_transition(self, transition):
        """Function to change only aerotech position to what it uses for mse and sa
        Also moves cryostream out of the field of view for the top camera"""
        gmy_offset, gmz_offset = self.aerotech_offsets()
        cryo_sa, cryo_mse = self.cryo_positions()
        if transition == "sa2mse":
            self.dgmy.put(-gmy_offset)
            time.sleep(0.1)
            self.dgmz.put(-gmz_offset)
            time.sleep(0.1)
            self.cryo.put(cryo_mse)
            self.colli.put(-20.0)
        elif transition == "mse2sa":
            self.dgmy.put(gmy_offset)
            time.sleep(0.1)
            self.dgmz.put(gmz_offset)
            time.sleep(0.1)
            self.cryo.put(cryo_sa)


class SampleMotorX06SA(object):
    def __init__(self):
        """Beamline motors class for ALC"""
        self.smargon_installed = "smargon" == config["goniometer"].lower()
        self.single_axis_installed = "single-axis" == config["goniometer"].lower()

        if self.smargon_installed:
            # Smargon co-ordinates for ALC jobs - x = SHZ, y = SHY, z = SHX
            # Beamline co-ordinates for gridscans - x = BX, y = BY, z = BZ
            self.smargon = smargon.SmarGon()

        elif self.single_axis_installed:
            # relative movement
            self.dx = epics.PV("X06SA-ES-DF1:GMX-INCP")
            self.dy = epics.PV("X06SA-ES-DFS:TRY1.RLV")
            self.dz = epics.PV("X06SA-ES-DFS:TRZ1.RLV")
            # absolute movement
            self.x = epics.PV("X06SA-ES-DF1:GMX-VAL")
            self.y = epics.PV("X06SA-ES-DFS:TRY1")
            self.z = epics.PV("X06SA-ES-DFS:TRZ1")
            # actual position
            self.xrbv = epics.PV("X06SA-ES-DF1:GMX-RBV")
            self.yrbv = epics.PV("X06SA-ES-DFS:TRY1.RBV")
            self.zrbv = epics.PV("X06SA-ES-DFS:TRZ1.RBV")
            # movement finishes?
            self.xdone = epics.PV("X06SA-ES-DF1:GMX-DONE")
            self.ydone = epics.PV("X06SA-ES-DFS:TRY1.DMOV")
            self.zdone = epics.PV("X06SA-ES-DFS:TRZ1.DMOV")

        # Relative movement of aerotech for top camera calibration
        self.dgmx = epics.PV("X06SA-ES-DF1:GMX-INCP")
        self.dgmy = epics.PV("X06SA-ES-DF1:GMY-INCP")
        self.dgmz = epics.PV("X06SA-ES-DF1:GMZ-INCP")

        # Collimator
        self.colli = epics.PV("X06SA-ES-MIC:TRY2.VAL")

        # Lamp
        self.lamp_get = epics.PV("X06SA-ES-KOI:GET-POS")
        self.lamp_set = epics.PV("X06SA-ES-KOI:SET-POS")

        # font light
        self.front_light = epics.PV("X06SA-ES-FL1:SET-LIGHT")

        # cryo
        self.cryo = epics.PV("X06SA-ES-CJ:TRY1.VAL")

        # hubclient to access beamline specific parameters
        self.hub = hubclient.Hub(beamline.lower())

        # Sample Camera - zoom motor and mapping to DA+ GUI
        self.zoom_set = epics.PV("X06SA-ES-M1:ROY1")
        self.zoom_get = epics.PV("X06SA-ES-M1:ROY1.RBV")
        self.current_zoom_level = self.zoom_get.get()
        self.gui_zoom_levels = {
            1: 0.1,
            1.4: 2.0,
            2.0: 4.0,
            2.9: 6.0,
            4.1: 8.0,
            5.9: 10.0,
            7.7: 11.5,
            10.0: 13.0,
        }

        # Camera translation factors in float format for pixel to mm conversion
        self.camera_translation_factor_a = self.hub.getd("camera_translationFactorA")
        self.camera_translation_factor_b = self.hub.getd("camera_translationFactorB")

    def setup(self):
        """move down lamp to get a dark background and set front light to full"""
        self._lamp_pos = self.lamp_get.get(as_string=True)
        self.lamp_set.put("Measure")
        self.front_light.put(5)

    def restore(self):
        """restore lamp position to standard state"""
        self.lamp_set.put(self._lamp_pos)

    def moveto(self, x, y, z):
        if self.smargon_installed:
            """ALC - smargon uses Smargon Coordinate System (SCS)"""
            scs_target = {"SHX": z, "SHY": y, "SHZ": x, "CHI": 0.0, "PHI": 0.0}
            logger.info(f"Moving SmarGon: {scs_target}")
            self.smargon.scs = scs_target
            self.smargon.wait()
        elif self.single_axis_installed:
            self.x.put(x, wait=True)
            self.y.put(y, wait=True)
            self.z.put(z, wait=True)

    def moveby(self, dx, dy, dz):
        if self.smargon_installed:
            """ALC - smargon uses SCS coordinates"""
            start = self.smargon.readback_scs()
            x = dx + start["SHZ"]
            y = dz + start["SHY"]
            z = dy + start["SHX"]
            self.moveto(x, y, z)
        elif self.single_axis_installed:
            self.dx.put(dx)
            self.dy.put(dy)
            self.dz.put(dz)
            self.wait()

    def moveby_in_cryozone(self, dx, dy, dz):
        if self.smargon_installed:
            """Moves Smargon within cryozone limits. x- is always the same, y,z- depend on Omega. Limits are in um"""
            xlimit = [15.5, 21.5]  # smargon limits are 10 - 25 um
            ylimit = [-2.5, 2.5]
            zlimit = ylimit

            start = self.smargon.readback_scs()
            x = start["SHZ"]
            y = start["SHY"]
            z = start["SHX"]

            if (
                (xlimit[0] < dx + x < xlimit[1])
                and (ylimit[0] < dy + y < ylimit[1])
                and (zlimit[0] < dz + z < zlimit[1])
            ):
                self.moveby(dx, dy, dz)

        elif self.single_axis_installed:
            # Moves the smaracts within cryozone limits (in um)
            # x-cooridnate is always the same, y,z- depends on Omega
            xlimit = [-22, -16]
            ylimit = [-2, 2]
            zlimit = [-2, 2]

            x = self.xrbv.get()
            y = self.yrbv.get()
            z = self.zrbv.get()

            if (
                (xlimit[0] < dx + x < xlimit[1])
                and (ylimit[0] < dy + y < ylimit[1])
                and (zlimit[0] < dz + z < zlimit[1])
            ):
                self.moveby(dx, dy, dz)

    def movetoraster(self, x, y, z):
        if self.smargon_installed:
            """Raster - smargon uses Beamline Coordinate System (BCS)"""
            target = (x, y, z)
            logger.info(f"Moving SmarGon to first cell of raster scan: {target}")
            self.smargon.xyz(target)
        elif self.single_axis_installed:
            logger.info("movetoraster not implemented for single-axis")

    def movebyraster(self, dx, dy, dz):
        if self.smargon_installed:
            # Raster - smargon uses Beamline Coordinate System (BCS)
            start = self.smargon.readback_bcs()
            x = dx + start["BX"]
            y = dy + start["BY"]
            z = dz + start["BZ"]
            target = (x, y, z)
            logger.info(f"Moving SmarGon to first cell of raster scan: {target}")
            self.smargon.xyz(target)
        elif self.single_axis_installed:
            logger.info("movebyraster not implemented for single-axis")

    def movetoraster(self, x, y, z):
        """Raster - smargon uses Beamline Coordinate System (BCS)"""
        target = (x, y, z)
        logger.info(f"Moving SmarGon to first cell of raster scan: {target}")
        self.smargon.xyz(target)

    def movebyraster(self, dx, dy, dz):
        # Raster - smargon uses Beamline Coordinate System (BCS)
        start = self.smargon.readback_bcs()
        x = dx + start["BX"]
        y = dy + start["BY"]
        z = dz + start["BZ"]
        target = (x, y, z)
        logger.info(f"Moving SmarGon to first cell of raster scan: {target}")
        self.smargon.xyz(target)

    def wait(self):
        if self.smargon_installed:
            time.sleep(0.5)  # wait for a status to change?
        elif self.single_axis_installed:
            while self.xdone.get() == 0 or self.ydone.get() == 0 or self.zdone.get() == 0:
                time.sleep(0.1)
            time.sleep(0.2)  # wait for final issued motor movement to finish?

    def pixel_to_mm(self, pixel):
        """Calculate the pixel_to_mm conversion using camera translation factors and zoom level"""
        pixel = np.array(pixel)
        logger.info(f"X06SA pixel is: {pixel}")
        zoom = self.currentZoom()
        logger.info(f"X06SA zoom is: {zoom}")
        conversion = np.exp(self.camera_translation_factor_b + (self.camera_translation_factor_a * zoom))
        logger.info(f"X06SA conversion is: {conversion}")
        logger.info(f"X06SA pixel_to_mm is: {pixel / conversion}")
        return pixel / conversion

    def zoomto(self, zoom_level):
        """zoomto using Gui zoom levels"""
        try:
            motor_target = self.gui_zoom_levels[zoom_level]
        except KeyError:
            raise Exception("Available zoom levels: ", self.gui_zoom_levels.keys())

        self.current_zoom_level = motor_target
        self.zoom_set.put(motor_target, wait=True)

    def zoomto(self, motor_target):
        """zoomto using motor zoom levels"""

        self.current_zoom_level = motor_target
        self.zoom_set.put(motor_target, wait=True)

    def guiZoomToMotor(self, guiZoom):
        try:
            return self.gui_zoom_levels[guiZoom]
        except KeyError:
            raise Exception("Available zoom levels: ", self.gui_zoom_levels.keys())

    def currentZoom(self):
        # default timeout = 1second increased to compensate for network issues/slowness
        self.current_zoom_level = self.zoom_get.get(timeout=2)
        return self.current_zoom_level

    def num_zoom_levels(self):
        return len(self.zoom_levels)

    def aerotech_offsets(self):
        """Aerotech offsets applied during escape state changes - MSE and SA"""
        gmy_offset = self.hub.getd("escape_GmyMeasurementPosition")
        gmz_offset = self.hub.getd("escape_GmzMeasurementPosition")
        return gmy_offset, gmz_offset

    def cryo_positions(self):
        """cryostream positions for escape states Sample Alignment and Manual Sample Exchange"""
        cryo_sa = self.hub.get("escape_CryojetMeasurementPosition")
        cryo_mse = self.hub.get("escape_CryojetParkPosition")
        return cryo_sa, cryo_mse

    def loop_escape_transition(self, transition):
        """Change aerotech position only using offsets for mse and sa -
        move cryostream out of the top camera field of view"""
        gmy_offset, gmz_offset = self.aerotech_offsets()
        cryo_sa, cryo_mse = self.cryo_positions()
        if transition == "sa2mse":
            self.dgmy.put(-gmy_offset)
            time.sleep(0.1)
            self.dgmz.put(-gmz_offset)
            time.sleep(0.1)
            self.cryo.put(cryo_mse)
            self.colli.put(-20.0)
        elif transition == "mse2sa":
            self.dgmy.put(gmy_offset)
            time.sleep(0.1)
            self.dgmz.put(gmz_offset)
            time.sleep(0.1)
            self.cryo.put(cryo_sa)


class SampleMotorX06DA(object):
    def __init__(self):
        # Prigo co-ordinates for automatic loop centering jobs
        self.xrbv = epics.PV("X06DA-ES-PRIGO:SHZ_RBV")
        self.yrbv = epics.PV("X06DA-ES-PRIGO:SHY_RBV")
        self.zrbv = epics.PV("X06DA-ES-PRIGO:SHX_RBV")

        self.x = epics.PV("X06DA-ES-PRIGO:SHZ")
        self.y = epics.PV("X06DA-ES-PRIGO:SHY")
        self.z = epics.PV("X06DA-ES-PRIGO:SHX")

        self.busy = epics.PV("X06DA-ES-PRIGO:BUSY")

        # Beamline co-ordinates for grid scan step of SDU
        self.blxrbv = epics.PV("X06DA-ES-PRIGO:BLX_RBV")
        self.blyrbv = epics.PV("X06DA-ES-PRIGO:BLY_RBV")
        self.blzrbv = epics.PV("X06DA-ES-PRIGO:BLZ_RBV")
        self.button = epics.PV("X06DA-ES-PRIGO:BEAM2PRIGO.PROC")

        self.blx = epics.PV("X06DA-ES-PRIGO:BLX")
        self.bly = epics.PV("X06DA-ES-PRIGO:BLY")
        self.blz = epics.PV("X06DA-ES-PRIGO:BLZ")

        # {GUI-zoom-level:motor value}
        self.gui_zoom_levels = {1: -800, 2: -603, 5: -208, 8: -10}
        self.zoom_motor = epics.PV("X06DA-ES-MS:ZOOM")
        self.hub = hubclient.Hub(beamline.lower())

        self.current_zoom_level = self.zoom_motor.get()  # current zoom motor position

    def setup(self):
        pass

    def restore(self):
        pass

    def moveto(self, x, y, z):
        self.x.put(x)
        self.y.put(y)
        self.z.put(z)

    def movetoraster(self, x, y, z):
        # Moves the prigo to first cell of raster scan
        self.blx.put(x)
        self.bly.put(y)
        self.blz.put(z)
        self.button.put(1)
        time.sleep(2.0)

    def moveby(self, dx, dy, dz):
        self.x.put(dx + self.xrbv.get())
        self.y.put(dy + self.yrbv.get())
        self.z.put(dz + self.zrbv.get())
        # self.wait()

    def movebyraster(self, dx, dy, dz):
        # Moves the prigo to first cell of raster scan
        self.blx.put(dx + self.blxrbv.get())
        self.bly.put(dy + self.blyrbv.get())
        self.blz.put(dz + self.blzrbv.get())

    def moveby_in_cryozone(self, dx, dy, dz):
        # Moves the prigo within cryozone limits. x-cooridnate is always the same, y,z- depends on Omega
        # Limits in um
        xlimit = [15.5, 21.7]
        ylimit = [-3.3, 2.8]
        zlimit = ylimit

        x = self.xrbv.get()
        y = self.yrbv.get()
        z = self.zrbv.get()

        if (xlimit[0] < dx + x < xlimit[1]) and (ylimit[0] < dy + y < ylimit[1]) and (zlimit[0] < dz + z < zlimit[1]):
            self.moveby(dx, dy, dz)

    def wait(self):
        time.sleep(0.3)  # Wait a while before checking the channel to make sure value is changed
        while self.busy.get() == 1:
            time.sleep(0.1)

    def pixel_to_mm(self, pixel):
        pixel = np.array(pixel)
        logger.info("X06DA pixel is: {}".format(pixel))
        zoom = self.currentZoom()
        logger.info("X06DA zoom is: {}".format(zoom))
        # Get the parameters of the polynomial equation from the HUB
        a = float(self.hub.find("camera_translationFactorA")["camera_translationFactorA"])
        b = float(self.hub.find("camera_translationFactorB")["camera_translationFactorB"])
        c = float(self.hub.find("camera_translationFactorC")["camera_translationFactorC"])
        conversion = (a * (zoom**2)) + (b * zoom) + c
        logger.info("X06DA conversion is: {}".format(conversion))
        logger.info("X06DA pixel_to_mm is: {}".format(pixel / conversion))
        return pixel / conversion

    def getZoom(self):
        self.current_zoom_level = self.zoom_motor.get()
        return self.current_zoom_level

    def zoomto(self, zoom):
        # Use zoom-motor values to zoom
        self.current_zoom_level = zoom
        self.zoom_motor.put(zoom, wait=True)

    def currentZoom(self):
        # default timeout = 1second increased to compensate for network issues/slowness
        self.current_zoom_level = self.zoom_get.get(timeout=2)
        return self.current_zoom_level


class OmegaMotor(object):
    def __init__(self, prefix):
        # omega movement
        self.omega_inc = epics.PV(prefix + "-ES-DF1:OMEGA-INCP")
        self.omega_get = epics.PV(prefix + "-ES-DF1:OMEGA-GETP")
        self.omega_set = epics.PV(prefix + "-ES-DF1:OMEGA-VAL")
        self.omega_done = epics.PV(prefix + "-ES-DF1:OMEGA-DONE")
        self.omega_lock = epics.PV(prefix + "-ES-DF1:LOCK")
        self.omega_direct = epics.PV(prefix + "-ES-DF1:MODE-DIRECT")

        # self.omega_done.setMonitor()  # Kuba what is this?

    def wait(self):
        timeout1 = time.time() + 10
        # wait omega starts
        while self.omega_done.get() == 1 and time.time() < timeout1:
            pass
        # wait omega stops
        timeout2 = time.time() + 30
        while self.omega_done.get() == 0 and time.time() < timeout2:
            time.sleep(0.01)
        if self.omega_done.get() == 0:
            logger.info("Timeout on OmegaMotor wait function")
            raise Exception

    def moveto(self, omega):
        self.omega_set.put(omega)
        self.wait()

    def moveby(self, omegaInc):
        # movement by 0 often puts aerotech in funny state
        if omegaInc == 0:
            logger.info("Omega already in position. No movement issued.")
        else:
            logger.info(f"Moving OmegaMotor by increment: {omegaInc}")
            self.omega_inc.put(omegaInc)
            self.wait()

    def set_locked(self):
        self.omega_lock.put(1)
        time.sleep(0.1)

    def set_direct(self):
        self.omega_direct.put(1)
        time.sleep(0.1)

    def getOmega(self):
        # Round float to 4th digits
        return float("{0:.4f}".format(self.omega_get.get()))


def zooms_arg_callback(option, opt_str, val, parser):
    setattr(parser.values, option.dest, [int(s) for s in val.split(",")])


class SampleMotorDummy(object):
    def __init__(self, zoomLevel):
        self.xrbv = None
        self.yrbv = None
        self.zrbv = None

        self.x = None
        self.y = None
        self.z = None

        self.busy = None

        # (zoom motor position, um/pixel, beam horizontal position)
        self.zoom_levels_calibration = [(-355, 1.83, 581), (-72, 0.81, 600)]

        # {GUI zoom-level:motor value}
        self.zoom_levels = {1: -800, 2: -602.5, 5: -207.5, 8: -10}

        self.zoom_motor = None

        self.current_zoom_level = -602.5

        self.conversionParams = self.__pixel_to_mm_lineq()

    def setup(self):
        pass

    def restore(self):
        pass

    def moveto(self, x, y, z):
        self.x.put(x)
        self.y.put(y)
        self.z.put(z)

    def moveby(self, dx, dy, dz):
        self.x.put(dx + self.xrbv.get())
        self.y.put(dy + self.yrbv.get())
        self.z.put(dz + self.zrbv.get())
        self.wait()

    def wait(self):
        time.sleep(0.2)  # Wait a while before checking the channel to make sure value is chaned
        while self.busy.get() == 1:
            time.sleep(0.1)

    def __pixel_to_mm_lineq(self):
        # Solving set of linear equations y1 = a*x1 + 1*b
        x1 = self.zoom_levels_calibration[0][0]
        y1 = self.zoom_levels_calibration[0][1]
        x2 = self.zoom_levels_calibration[1][0]
        y2 = self.zoom_levels_calibration[1][1]
        print(x1, y1)
        print(x2, y2)
        x = np.array([[x1, 1], [x2, 1]])
        y = np.array([y1, y2])

        a, b = np.linalg.solve(x, y)
        print(a, b)
        return (a, b)

    # def pixel_to_mm(self, pixel):
    #     conversion = self.zoom_levels[self.current_zoom_level][1]
    #     return pixel * conversion / 1000.

    def pixel_to_mm(self, pixel):
        pixel = np.array(pixel)
        a = self.conversionParams[0]
        b = self.conversionParams[1]
        conversion = (a * self.current_zoom_level) + b
        pixel = pixel * conversion
        return pixel / 1000.0

    def zoomto(self, zoom_level):
        try:
            motor_target = self.zoom_levels[zoom_level]
        except KeyError:
            raise Exception("Available zoom levels: ", self.zoom_levels.keys())

        self.current_zoom_level = zoom_level

        self.zoom_motor.put(motor_target, wait=True)

    # def zoomto(self, zoom_level):
    #     global beam_ref_h
    #     beam_ref_h = self.zoom_levels[zoom_level][2]
    #     self.current_zoom_level = zoom_level
    #     motor_pos = self.zoom_levels[zoom_level][0]
    #     self.zoom_motor.put(motor_pos, wait=True)

    def num_zoom_levels(self):
        return len(self.zoom_levels)


#### 2018-09-18
#### OLD class to use with single axis goniometer at X06DA beamline

# class SampleMotorX06DA(object):
#     def __init__(self):
#         # relative movement
#         self.dx = epics.PV('X06DA-ES-DF1:GMX-INCP')
#         self.dy = epics.PV('X06DA-ES-DFS:TRY1.RLV')
#         self.dz = epics.PV('X06DA-ES-DFS:TRZ1.RLV')
#         # absolute movement
#         self.x = epics.PV('X06DA-ES-DF1:GMX-VAL')
#         self.y = epics.PV('X06DA-ES-DFS:TRY1')
#         self.z = epics.PV('X06DA-ES-DFS:TRZ1')
#         # actuale position
#         self.xrbv = epics.PV('X06DA-ES-DF1:GMX-RBV')
#         self.yrbv = epics.PV('X06DA-ES-DFS:TRY1.RBV')
#         self.zrbv = epics.PV('X06DA-ES-DFS:TRZ1.RBV')
#         # movement finishes?
#         self.xdone = epics.PV('X06DA-ES-DF1:GMX-DONE')
#         self.ydone = epics.PV('X06DA-ES-DFS:TRY1.DMOV')
#         self.zdone = epics.PV('X06DA-ES-DFS:TRZ1.DMOV')


#         # {GUI-zoom-level:motor value}
#         # self.gui_zoom_levels = {1:-800, 2:-603, 5:-208, 8:-10}
#         self.zoom_motor = epics.PV('X06DA-ES-MS:ZOOM')

#         self.current_zoom_level = self.zoom_motor.get() # current zoom motor position

#     def setup(self):
#         pass

#     def restore(self):
#         pass

#     def moveto(self, x, y, z):
#         self.x.put(x)
#         self.y.put(y)
#         self.z.put(z)

#     def moveby(self, dx, dy, dz):
#         self.x.put(dx + self.xrbv.get())
#         self.y.put(dy + self.yrbv.get())
#         self.z.put(dz + self.zrbv.get())
#         # self.wait()

#     def moveby_in_cryozone(self, dx, dy, dz):
#         # Moves the prigo within cryozone limits. x-cooridnate is always the same, y,z- depends on Omega
#         # Limits in um
#         xlimit = [15.5, 21.7]
#         ylimit = [-3.3, 2.8]
#         zlimit = ylimit

#         x = self.xrbv.get()
#         y = self.yrbv.get()
#         z = self.zrbv.get()

#         if (xlimit[0] < dx+x < xlimit[1]) and (ylimit[0] < dy+y < ylimit[1]) and (zlimit[0] < dz+z < zlimit[1]):
#             self.moveby(dx,dy,dz)

#     def wait(self):
#         time.sleep(0.3) # Wait a while before checking the channel to make sure value is changed
#         while(self.xdone.get() == 0 or
#               self.ydone.get() == 0 or
#               self.zdone.get() == 0):
#             time.sleep(0.1)

#     def pixel_to_mm(self, pixel):
#         pixel = np.array(pixel)
#         zoom = self.currentZoom()
#         # Get the parameters of the polynomial equation from the HUB
#         hub = hubclient.Hub(beamline='X06DA')
#         a = float(hub.find('camera_translationFactorA')['camera_translationFactorA'])
#         b = float(hub.find('camera_translationFactorB')['camera_translationFactorB'])
#         c = float(hub.find('camera_translationFactorC')['camera_translationFactorC'])
#         conversion = (a * (zoom**2)) + (b * zoom) + c
#         return pixel / conversion

#     def getZoom(self):
#         self.current_zoom_level = self.zoom_motor.get()
#         return self.current_zoom_level

#     def zoomto(self, zoom):
#         # Use zoom-motor values to zoom
#         self.current_zoom_level = zoom
#         self.zoom_motor.put(zoom, wait=True)

#     def currentZoom(self):
#         self.current_zoom_level = self.zoom_motor.get()
#         return self.current_zoom_level

#     # def zoomtoGUI(self, zoom_level):
#     #     try:
#     #         motor_target = self.gui_zoom_levels[zoom_level]
#     #     except KeyError:
#     #         raise Exception('Available zoom levels: ', self.gui_zoom_levels.keys())

#     #     self.current_zoom_level = motor_target
#     #     self.zoom_motor.put(motor_target, wait=True)

#     # def guiZoomToMotor(self, guiZoom):
#     #     try:
#     #         return self.gui_zoom_levels[guiZoom]
#     #     except KeyError:
#     #         raise Exception('Available zoom levels: ', self.gui_zoom_levels.keys())

#     # def num_zoom_levels(self):
#     #     return len(self.zoom_levels)
