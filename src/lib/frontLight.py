import epics


class frontLight(object):
    def __init__(self, beamline):
        """Sets up epics channel access for sample front light"""
        self.beamline = beamline.upper()

        self.front_light_channel = {
            "X06DA": "-ES-FL:SET-BRGHT",
            "X06SA": "-ES-FL1:SET-LIGHT",
            "X10SA": "-ES-FL1:SET-LIGHT",
        }

        if self.beamline not in self.front_light_channel.keys():
            raise Exception(f"Front Light: Beamline needs to be one of: {list(self.front_light_channel.keys())}")

        self.fl = epics.PV(self.beamline + self.front_light_channel[self.beamline])

    def setup(self):
        """Set Front Light to full power for use in prelocate worker"""
        if self.beamline == "X06SA":
            self.fl.put(5.0)
        elif self.beamline == "X10SA":
            self.fl.put(0.2)
        elif self.beamline == "X06DA":
            pass

    def restore(self):
        """Switch off front light at end of prelocate worker"""
        if self.beamline == "X06DA":
            pass
        else:
            self.fl.put(0)


if __name__ == "__main__":
    fl = frontLight("X10SA")
    fl.setup()
