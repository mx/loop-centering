import epics


class backLight(object):
    def __init__(self, beamline):
        """Sets up epics channel access for sample back light used by sample camera"""
        self.beamline = beamline.upper()

        self.back_light_channel = {
            "X06DA": "-ES-BL:SET-POS",
            "X06SA": "-ES-KOI:SET-LIGHT",
            "X10SA": "-ES-KOI:SET-LIGHT",
        }

        self.back_light_position = {
            "X06DA": "-ES-BL:SET-POS",
            "X06SA": "-ES-KOI:SET-POS",
            "X10SA": "-ES-KOI:SET-POS",
        }

        if self.beamline not in self.back_light_channel.keys():
            raise Exception(f"Back Light: Beamline needs to be one of: {list(self.back_light_channel.keys())}")

        self.bl = epics.PV(self.beamline + self.back_light_channel[self.beamline])
        self.lamp = epics.PV(self.beamline + self.back_light_position[self.beamline])

    def setup(self):
        """Set the following Back light settings at start of prelocate worker
        X06SA - turn backlight OFF.
        X10SA - turn backlight ON.
        X06DA - there is no backlight"""
        if self.beamline == "X06SA":
            self.bl.put(0)
        elif self.beamline == "X10SA":
            self.bl.put(3.0)
        elif self.beamline == "X06DA":
            pass
        self.lamp.put("Measure")

    def restore(self):
        """Set Back light to full power at end of prelocate worker"""
        if self.beamline == "X06SA":
            self.bl.put(5.0)
        elif self.beamline == "X10SA":
            self.bl.put(3.0)
        elif self.beamline == "X06DA":
            pass


if __name__ == "__main__":
    bl = backLight("X10SA")
    bl.setup()
