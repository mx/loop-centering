#!/usr/bin/env python

import importlib
import logging
import os
import redis

import beamlineUtils as bu
import globalStates as state
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("AlcDriver")

import redis_conf

try:
    import beamlineMotors as beamlineMotors
    import sampleCamera as sampleCamera
    import frontLight as frontLight
    import backLight as backLight
    import topCamera as topCamera
except:
    logger.info(" ** BeamlineMotors, sampleCamera and frontLight modules not loaded ** ")


class AlcDriver(object):
    """Main ALC class used to start and control worker threads."""

    def __init__(self, beamline):
        try:
            self.beamline = bu.validateBeamline(beamline)
        except Exception as e:
            msg = f"Cannot find beamline name: {e}"
            logger.info(msg)
            raise Exception(msg)

        logger.info(f"Running on beamline: {self.beamline}")
        # Access to REDIS server
        self.redis_host = redis_conf.redis_host
        self.redis_port = redis_conf.redis_port
        self.redis_db = redis_conf.redis_db[self.beamline]

        logger.info(f"Initalizing State and Runtime variables in REDIS at {self.redis_host}:{self.redis_port}")
        try:
            self._r = redis.StrictRedis(host=self.redis_host, port=self.redis_port, db=self.redis_db)
            self._r.set(state.abort, state.abortFalse)
            self._r.set(state.busy, state.busyFalse)
            self._r.set(state.currentWorker, state.currentWorkerNone)
            self._r.set(workerRuntime.workerName, workerRuntime.workerNameNone)
            self._r.set(workerRuntime.workerResult, workerRuntime.workerResultNone)
            self._r.set(workerRuntime.workerErrorMsg, workerRuntime.workerErrorMsgNone)
            self._r.set(workerRuntime.workerReturnData, workerRuntime.workerReturnDataNone)
        except redis.ConnectionError:
            msg = f"Redis service at {self.redis_host}:{self.redis_port} is not available"
            logger.info(msg)
            raise Exception(msg)

        self.motors = {}
        if beamline != "BOGUS":
            logger.info("Initializing EPICS channels")
            try:
                logger.info(self.beamline)
                logger.info("before the end..")
                self.motors["samCam"] = sampleCamera.sampleCamera(self.beamline)
            except Exception as e:
                raise Exception(f"Error initializing SampleCamera module. Reason: {e}")

            try:
                self.motors["topCam"] = topCamera.topCamera(self.beamline)
            except Exception as e:
                raise Exception(f"Error initializing TopCamera module. Reason: {e}")

            try:
                self.motors["sampleMotor"] = eval("beamlineMotors.SampleMotor%s()" % self.beamline)
            except Exception as e:
                raise Exception(f"Error initializing SampleMotor module. Reason: {e}")

            try:
                self.motors["omegaMotor"] = beamlineMotors.OmegaMotor(self.beamline)
            except Exception as e:
                raise Exception(f"Error initializing OmegaMotor module. Reason: {e}")

            try:
                self.motors["frontLight"] = frontLight.frontLight(self.beamline)
            except Exception as e:
                raise Exception(f"Error initializing frontLight module. Reason: {e}")

            try:
                self.motors["backLight"] = backLight.backLight(self.beamline)
            except Exception as e:
                raise Exception(f"Error initializing backLight module. Reason: {e}")

            logger.info("Finished initializing EPICS channels.")

        logger.info("AlcDriver started")

    def abort(self):
        """Abort currently running worker thread"""
        # TODO: do we need to setit up in atomic way
        self._r.set(state.abort, state.abortTrue)

    def busy(self):
        """Check if some worker running"""
        return self._r.get(state.busy).decode("utf-8") == state.busyTrue

    def __setBusy(self):
        """Sets busy=True using Redis WATCH/TRANSACTION facility to ensure thread-safe operation"""
        with self._r.pipeline() as pipe:
            try:
                pipe.watch(state.busy)
                if pipe.get(state.busy).decode("utf-8") == state.busyTrue:
                    # TODO or thread with a name center_on_tip is running. Do we need to check also ABORT?
                    raise Exception("Trying to change state to BUSY=True: failed - BUSY is already True")

                pipe.multi()
                pipe.set(state.busy, state.busyTrue)
                pipe.set(state.abort, state.abortFalse)
                pipe.execute()
            except redis.WatchError:
                raise Exception(
                    "Trying to change state to BUSY=True: failed - Could not set BUSY to True. It was changed from somewhere else."
                )

    def listWorkers(self):
        workerpath = f"{self.beamline}-workers"
        workers_tmp = [f for f in os.listdir(workerpath) if os.path.isfile(os.path.join(workerpath, f))]
        workers = []
        for w in workers_tmp:
            if w[-1] != "c":  # and w.find('bogus') < 0:  # remove .pyc
                workers.append(w[:-3])
        return sorted(workers)

    def run(self, workerName, *args, **kwargs):
        if self.busy():
            msg = "Other worker process is already running. Can not run another process."
            logger.info(msg)
            return False

        try:
            workerModule = importlib.import_module(workerName)
            worker = getattr(workerModule, workerName)(
                beamline=self.beamline,
                motors=self.motors,
                redis_host=self.redis_host,
                redis_port=self.redis_port,
                redis_db=self.redis_db,
                *args,
                **kwargs,
            )
        # except AttributeError:
        except Exception as e:
            workersList = self.listWorkers()
            msg = f"Could not find worker {workerName}. Error: {e}. Known workers: {workersList}"
            logger.info(msg)
            raise Exception(msg)

        try:
            self.__setBusy()  # Set state to busy in thread-safe way
            logger.info("State changed to Busy=True successfully.")
        except Exception as e:
            logger.info(e)
            raise Exception(e)

        # if __setBusy was successful, we are ready to safely run the worker process
        worker.start()

        logger.info("ALC worker started {}".format(workerName))
        # Don't wait for thread to finish. Thread must clean up after itself.
        return True


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="SLS MX Automatic Loop Centering")
    parser.add_argument("-b", "--beamline", help="Beamline name (X06DA, X06SA, X10SA)", type=str)

    args.parser.parser_args()
    beamline = args.beamline

    logging.basicConfig(format="%(asctime)s - %(levelname)s : %(message)s")
    # Implement ...
