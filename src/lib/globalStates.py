# Global states
busy = "busy"
busyTrue = "True"
busyFalse = "False"

abort = "abort"
abortTrue = "True"
abortFalse = "False"

currentWorker = "currentWorker"
currentWorkerNone = "None"
