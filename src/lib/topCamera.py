import cv2
import numpy as np
import requests


class topCamera(object):
    """
    Class to operate Top Camera
    """

    def __init__(self, beamline):
        self.beamline = beamline.upper()

        self.cameras = {
            "X06SA": "http://axis-accc8ea5e463.psi.ch/jpg/1/image.jpg",
            "X06DA": "http://axis-accc8ed2972e.psi.ch/jpg/1/image.jpg",
            "X10SA": "http://x10sa-axis-3.psi.ch/jpg/4/image.jpg",
        }

        self.camera_pix_to_mm = {
            "X06SA": 1 / 36.0,
            "X06DA": "not_implemented",
            "X10SA": 1 / 50.0,
        }  # 1/34.0

        if self.beamline not in self.cameras.keys():
            raise Exception("TopCam: Beamline needs to be one of: %s" % str(self.cameras.keys()))

    def __toRGB(self, img):
        return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    def __rotate(self, img, angle):
        shape = img.shape
        cols = shape[0]
        rows = shape[1]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
        newImg = cv2.warpAffine(img, M, (cols, rows))
        return newImg

    def pixel_to_mm(self, pixel):
        """
        Converts pixels from in TopCam coordinate system to beamline motor millimeters

        :param pixel: list or array with [x,y]
        :return: mm - x,y cooridantes in milimeters
        """
        conversion = self.camera_pix_to_mm[self.beamline]
        pixel = np.array(pixel)
        mm = pixel * conversion
        return mm

    def getImage(self):
        """
        Gets the image from Top Camera @ self.beamline
        """
        r = requests.get(self.cameras[self.beamline])
        img = np.asarray(bytearray(r.content), dtype="uint8")
        img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        img = getattr(self, "processImage%s" % self.beamline)(img)
        return img

    def saveImage(self, img, filename):
        """
        Saves image to file
        :param img: numpy array
        :param filename: file name to save. Extension .jpg
        :return:
        """
        cv2.imwrite(filename, img)

    def processImageX06DA(self, img):
        # Specific operations to process the image from X06DA top camera
        return img

    def processImageX06SA(self, img):
        # Specific operations to process the image from X06SA top camera
        return img

    def processImageX10SA(self, img):
        # Specific operations to process the image from X10SA top camera
        return img
