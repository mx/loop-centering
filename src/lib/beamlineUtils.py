#!/usr/bin/env python
import os
import numpy as np

import hubclient
from mx_preferences import get_config

beamline = os.environ.get("BEAMLINE_XNAME")
config = get_config(beamline)
smargon_installed = "smargon" == config["goniometer"].lower()
single_axis_installed = "single-axis" == config["goniometer"].lower()


def validateBeamline(beamline):
    beamline = beamline.upper()
    beamlineList = ["X06DA", "X06SA", "X10SA", "BOGUS"]

    if beamline not in beamlineList:
        msg = f"Beamline needs to be one of {beamlineList}. Aborting..."
        raise Exception(msg)

    return beamline


def findBeamPosition(beamline, zoom):
    """Find beam position for a given zoom using
    least square fit based on the Hub information"""
    hubValue = hubclient.Hub(beamline.lower()).find("camera_beamPositions")
    positions = eval(hubValue["camera_beamPositions"])
    zooms = []
    beamX = []
    beamY = []
    for pos in positions:
        zooms.append(pos["zoom"])
        beamX.append(pos["x"])
        beamY.append(pos["y"])

    x = np.array(zooms)
    y1 = np.array(beamX)
    y2 = np.array(beamY)

    A = np.vstack([x**2, x, np.ones(len(x))]).T
    a1, b1, c1 = np.linalg.lstsq(A, y1, rcond=-1)[0]
    a2, b2, c2 = np.linalg.lstsq(A, y2, rcond=-1)[0]

    currentX = a1 * (zoom**2) + b1 * zoom + c1
    currentY = a2 * (zoom**2) + b2 * zoom + c2

    return np.array([currentX, currentY])


def rotation_matrix(omega):
    """
    Transformation around X axis
    :param omega:
    :return:
    """
    theta = np.radians(omega)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((1, 0, 0), (0, c, -s), (0, s, c)))
    return R


def xy2xyz(beamline, point, omega):
    if beamline.lower() == "x06da":
        return xy2xyz_x06da(point, omega)

    if beamline.lower() == "x06sa":
        return xy2xyz_x06sa(point, omega)

    if beamline.lower() == "x10sa":
        return xy2xyz_x10sa(point, omega)


def xy2xyz_x06da(point, omega):
    """Works for X06DA: Finds the proper x,y,z vector
    for motor move depending on omega angle"""
    x = point[0]
    y = point[1]
    vector = np.array([x, y, 0])

    R = rotation_matrix(-omega)  # Clockwise rotation, hence '-' sign

    res = R.dot(vector)

    return res


def xy2xyz_x06sa(point, omega):
    """Works for X06SA: Finds the proper x,y,z vector
    for motor move depending on omega angle"""
    x = point[0]
    y = point[1]

    vector = np.array([x, y, 0])
    if single_axis_installed:
        vector = vector * -1  # Uncomment when reverting to single axis goniometer

    R = rotation_matrix(omega)  # Clockwise rotation, hence '-' sign

    res = R.dot(vector)

    return res


def xy2xyz_x10sa(point, omega):
    """Implement for X10SA: Find proper x,y,z vector
    for motor move depending on omega angle"""
    x = point[0]
    y = point[1]

    vector = np.array([x, y, 0])
    # vector = vector * -1 # Uncomment when reverting to single axis goniometer

    R = rotation_matrix(omega)  # Clockwise rotation, hence '-' sign

    res = R.dot(vector)

    return res


def checkCryozone():
    pass
