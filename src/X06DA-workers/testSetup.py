#!/usr/bin/env python

# bl = os.environ.get('BEAMLINE_XNAME')
import Prigo
import logging
import os
import time
import workerBaseClass
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-testSetup")


class testSetup(workerBaseClass.workerBaseClass):
    """Prepares the beamline to the state as it was mounted by robot. Used in development"""

    def __init__(self, beamline, motors, redis_host, redis_port, redis_db, *args, **kwargs):
        super(testSetup, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        p = Prigo.Prigo()
        p.move_home()
        # p.wait() # Somehow this does not work witn alc conda env on X06DA
        time.sleep(3)
        print("Prigo Homed")
        zoom = -500
        self.omegaMotor.moveto(90)
        self.sampleMotor.zoomto(zoom)
        logger.info("Beamline in Cats mount-like setup.")
        return {}
