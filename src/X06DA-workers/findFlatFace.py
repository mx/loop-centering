#!/usr/bin/env python

import beamlineUtils as bu
import globalStates as state
import hubclient
import logging
import numpy as np
import scipy.optimize
import time
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

logger = logging.getLogger("Worker-findFlatFace")


class findFlatFace(workerBaseClass.workerBaseClass):
    """Set the loop in the microscope to face with biggest area"""

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        omegaStep=40,
        setupSamCam=True,
        restoreSamCam=True,
        apply_finishing_touches=True,
        debug=False,
        saveImg=True,
        start_from_current_omega=False,
        *args,
        **kwargs,
    ):
        """
        :param beamline:
        :param motors:
        :param redis_host:
        :param redis_port:
        :param redis_db:
        :param omegaStep:
        :param restoreSamCam:
        :param debug:
        :param saveImg:
        :param start_from_current_omega: if True starts from current Omage. If False, start from 0
        :param args:
        :param kwargs:
        """
        self.omegaStep = omegaStep

        self.omegaSteps = range(0, 180, omegaStep)

        self.start_from_current_omega = start_from_current_omega

        self.areas = []

        self.setupSamCam = setupSamCam
        self.restoreSamCam = restoreSamCam
        self.apply_finishing_touches = apply_finishing_touches
        self.debug = debug
        self.saveImg = saveImg
        self.hub = hubclient.Hub(beamline.lower())

        # Parameter to cut out the border from top and the bottom of the picture
        # Default: 110 - its around the half-width of the pin mounting the loop at zoom -500.0
        self.envelope_y = kwargs.pop("envelope_y", 110)

        super(findFlatFace, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

        if self.start_from_current_omega:
            omega = int(self.omegaMotor.getOmega())
            self.omegaSteps = range(omega, omega + 180, omegaStep)
        else:
            self.omegaSteps = range(0, 180, omegaStep)

    def findArea(self):
        """Finds the tip of the loop and moves it to the beamposition
        wait: if set to True, wait until the sample motor movement is finished
        debug: if True, will show the images with the contour and the tip"""

        zoom = self.sampleMotor.currentZoom()
        omega = self.omegaMotor.getOmega()

        logger.info("Finding area of the loop for Omega={} at zoom={}".format(omega, zoom))
        # Find beam position at current zoom level
        beamPos = bu.findBeamPosition(self.beamline, zoom)
        # Get image from camera
        img = self.samCam.getImage(gray=False)
        # Process image
        procImg = lip(img)
        procImg.findContour(zoom=zoom, beamline=self.beamline, hull=True)
        # Find the tip of the loop on processed image
        area = procImg.contourArea()
        # Find how cose to the edge is the tip

        # Save the image in the log directory
        if self.saveImg:
            procImg.saveContour()

        return area  # returns True/False

    def fit_sin(self, tt, yy):

        """Fit sin to the input time sequence, and return fitting parameters "amp", "omega", "phase", "offset", "freq", "period" and "fitfunc"
        https://stackoverflow.com/questions/16716302/how-do-i-fit-a-sine-curve-to-my-data-with-pylab-and-numpy
        """

        def sin_function(t, A, w, p, c):
            return A * np.sin(w * t + p) + c

        tt = np.array(tt)
        yy = np.array(yy)
        ff = np.fft.fftfreq(len(tt), (tt[1] - tt[0]))  # assume uniform spacing
        Fyy = abs(np.fft.fft(yy))
        guess_freq = abs(ff[np.argmax(Fyy[1:]) + 1])  # excluding the zero frequency "peak", which is related to offset
        guess_amp = np.std(yy) * 2.0**0.5
        guess_offset = np.mean(yy)
        guess = np.array([guess_amp, 2.0 * np.pi * guess_freq, 0.0, guess_offset])

        popt, pcov = scipy.optimize.curve_fit(sin_function, tt, yy, p0=guess)
        A, w, p, c = popt
        f = w / (2.0 * np.pi)
        fitfunc = lambda t: A * np.sin(w * t + p) + c
        return {
            "amp": A,
            "omega": w,
            "phase": p,
            "offset": c,
            "freq": f,
            "period": 1.0 / f,
            "fitfunc": fitfunc,
            "maxcov": np.max(pcov),
            "rawres": (guess, popt, pcov),
        }

    def sinfunc(self, t, A, w, p, c):
        # same as sin_function in fit_sin
        return A * np.sin(w * t + p) + c

    def find_max_angle_from_fit(self, res):
        max_angle = (np.pi / 2 - res["phase"]) / res["omega"]
        max_angle = max_angle % 360
        area = self.sinfunc(max_angle, res["amp"], res["omega"], res["phase"], 0)
        if area < 0:
            max_angle += 90
        return max_angle

    def finishing_touches(self):
        # Finishing touches #
        logger.info("Going to the end zoom")
        endZoom = self.hub.getd("alc_zoom_at_end")
        self.sampleMotor.zoomto(endZoom)

    def worker(self):
        """Rotates omage along the angles given in omegaSteps, and use sin_fit to find biggest ares."""

        if self.setupSamCam:
            logger.info("Setting Sample Camera to fixed gain and exposure")
            self.samCam.setup()

        # Reset areas storage just in case
        self.areas = []
        # Rotate and calculate areas
        for i, omegaInc in enumerate(self.omegaSteps):
            # Check if user aborted operation
            if self.is_aborted():
                return {}

            omega_start = time.time()

            self.omegaMotor.moveto(omegaInc)

            omega_stop = time.time()

            img_proc_start = time.time()
            area = self.findArea()
            img_proc_stop = time.time()
            if self.debug:
                print("Omega: {}, Processing: {}".format(omega_stop - omega_start, img_proc_stop - img_proc_start))
                logger.info(
                    "Omega: {}, Processing: {}".format(omega_stop - omega_start, img_proc_stop - img_proc_start)
                )
            self.areas.append(area)

        # Fit areas to sin curve and find angle for which area is the biggest from fitted curve
        try:
            res = self.fit_sin(self.omegaSteps, self.areas)
            max_angle = self.find_max_angle_from_fit(res)
            logger.info("Find max area from fit to be for angle {}.".format(max_angle))
        except RuntimeError:
            logger.info("findFlatFace: Error in fitting. Setting angle to -1")
            max_angle = -1

        logger.info("Moving omega to {}".format(max_angle))

        # Move omega to angle for which area is the biggest
        self.omegaMotor.moveto(max_angle)

        if self.restoreSamCam:
            logger.info("Restoring sample camera to automatic gain and exposure")
            self.samCam.restore()

        if self.apply_finishing_touches:
            self.finishing_touches()

        # Take an image after end zoom is reached
        if self.saveImg:
            logger.info("Saving image after final zoom was reached")
            img = self.samCam.getImage(gray=False)
            procImg = lip(img)
            # procImg.saveImage(directory='/home/logs/alc/images/zoom10')
            procImg.saveImage(directory="/scratch/kaminski_j/alc_images/images/zoom10")
        return {}
