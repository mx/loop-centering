#!/usr/bin/env python

import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-samCamManual")


class samCamManual(workerBaseClass.workerBaseClass):
    """Puts Sample Camera in manual gain and exposure mode"""

    def __init__(self, beamline, motors, redis_host, redis_port, redis_db, *args, **kwargs):
        super(samCamManual, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        self.samCam.setup()
        return {}
