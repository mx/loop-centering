#!/usr/bin/env python

import beamlineUtils as bu
import globalStates as state
import hubclient
import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

logger = logging.getLogger("Worker-noCentering")


class noCentering(workerBaseClass.workerBaseClass):
    """No centering is performed if this worker is selected"""

    def __init__(self, beamline, motors, redis_host, redis_port, redis_db, *args, **kwargs):
        super(noCentering, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        logger.info("No centering was performed as requested.")

        return {}
