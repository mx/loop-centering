#!/usr/bin/env python

import beamlineUtils as bu
import datetime
import globalStates as state
import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

logger = logging.getLogger("Worker-rotateAndPic")


class rotateAndPic(workerBaseClass.workerBaseClass):
    """Rotates Omega motor by omegaStep and takes a picture. Camera in Automatic mode"""

    def __init__(self, beamline, motors, redis_host, redis_port, redis_db, omegaStep=10, *args, **kwargs):
        self.omegaStep = omegaStep
        super(rotateAndPic, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        logger.info("Starting job rotateAndPic")
        startOmega = self.omegaMotor.getOmega()
        endOmega = startOmega + 360
        imageDir = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

        while self.omegaMotor.getOmega() <= endOmega:
            img = self.samCam.getImage(gray=False)
            procImg = lip(img)
            filename = "{}.jpg".format(self.omegaMotor.getOmega())
            procImg.saveImage(
                directory="/home/logs/alc/images/rotateAndPic/{}".format(imageDir),
                filename=filename,
            )
            self.omegaMotor.moveby(self.omegaStep)

        return {}
