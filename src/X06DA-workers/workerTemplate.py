#!/usr/bin/env python

import beamlineUtils as bu
import globalStates as state
import hubclient
import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

logger = logging.getLogger("Worker-wokerTemplate")


class workerTemplate(workerBaseClass.workerBaseClass):
    """Centers beam on the tip of the loop"""

    def __init__(self, beamline, motors, redis_host, redis_port, redis_db, some_varaible, *args, **kwargs):
        self.someVariable = some_varaible

        super(workerTemplate, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        """This is where the main code goes. **This needs to be here for worker to work**"""

        ### all motors are availabe in self
        #
        # omega = self.omegaMotor.getOmega()
        # self.sampleMotor
        # self.sampleMotor.zoomto(someZoom)

        # Call some method
        # self.someMethod()

        # Implement worker to look for abort command
        if self.stop():
            self.result = workerRuntime.workerResultAbort
            return

        # You don't need any try/except block. All errors are caught by the parent class
        return {}

    def someMethod(self):
        """Implement if needed"""
        pass
