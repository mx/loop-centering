#!/usr/bin/env python

import beamlineUtils as bu
import cv2
import globalStates as state
import hubclient
import logging
import numpy as np
import os
import time
import workerBaseClass
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-loopInBeam")


class loopInBeam(workerBaseClass.workerBaseClass):
    """
    Returns True/False if loop is in beam
    """

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        setupSamCam=False,
        restoreSamCam=False,
        debug=False,
        background_folder="backgrounds",
        background_image="0.jpg",
        *args,
        **kwargs,
    ):

        #
        self.setupSamCam = setupSamCam
        self.restoreSamCam = restoreSamCam
        self.debug = debug
        self.background_folder = background_folder
        self.background_image = background_image

        super(loopInBeam, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def make_roi(self, image, beam_pos_x, beam_pos_y, box_size_x=100, box_size_y=100):
        x = np.array([beam_pos_x - box_size_x, beam_pos_x + box_size_x])
        y = np.array([beam_pos_y - box_size_y, beam_pos_y + box_size_y])
        return image[y[0] : y[1], x[0] : x[1]]

    def is_loop_in_beam(self, inp_img):

        beam_x, beam_y = bu.findBeamPosition(self.beamline, zoom)

        path = os.path.join(self.background_folder, self.background_image)

        inp_img = self.make_roi(inp_img, beam_x, beam_y)

        bg_img = cv2.imread(path)

        if bg_img is None:
            raise Exception("loopInBeam: background image not found. {}".format(path))

        bg_img = self.make_roi(bg_img, beam_x, beam_y)

        diff = cv2.absdiff(inp_img, bg_img)

        mask = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)

        th = 50
        imask = mask > th

        canvas = np.zeros_like(bg_img, np.uint8)
        canvas[imask] = bg_img[imask]
        canvas = cv2.cvtColor(canvas, cv2.COLOR_BGR2GRAY)

        kernel = np.ones((5, 5), np.uint8)
        erosion = cv2.erode(canvas, kernel, iterations=1)
        canvas = erosion.copy()

        dilation = cv2.dilate(canvas, kernel, iterations=3)
        canvas = dilation.copy()

        hist = cv2.calcHist([canvas], [0], None, [2], [0, 256])
        # hist[0] -> black color.
        # hist[1] -> white color. If bigger than 0 , loop is there

        return True if hist[1] > 0 else False

    def worker(self):
        """

        :return:
        """

        if self.setupSamCam:
            logger.info("Setting Sample Camera to fixed gain and exposure")
            self.samCam.setup()

        logger.info("loopInBeam: zooming to 5x")
        if self.debug:
            start = time.time()
        self.sampleMotor.zoomto(-208)
        if self.debug:
            logger.info("loopInBeam: zooming time: {}".format(time.time() - start))
            print("loopInBeam: zooming time: {}".format(time.time() - start))

        img = self.samCam.getImage(gray=False)
        is_loop_in_beam = self.is_loop_in_beam(inp_img=img)

        self.samCam.restore()
        logger.info("Loop in beam detected: {}".format(is_loop_in_beam))

        return dict(loop_in_beam=is_loop_in_beam)
