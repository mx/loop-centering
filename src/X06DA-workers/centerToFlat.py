#!/usr/bin/env python

import beamlineUtils as bu
import datetime
import globalStates as state
import hubclient
import logging
import numpy
import scipy.optimize
import time
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

import centerOnTip
import findFlatFace
import findLoopBoundingBox
import findLoopThickness

logger = logging.getLogger("Worker-centerToFlat")


class centerToFlat(workerBaseClass.workerBaseClass):
    """Set the loop in the microscope to face with biggest area"""

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        restoreSamCam=True,
        debug=True,
        saveImg=True,
        end_with_center_on_tip=True,
        *args,
        **kwargs,
    ):

        #
        self.restoreSamCam = restoreSamCam
        self.end_with_center_on_tip = end_with_center_on_tip
        self.debug = debug
        self.saveImg = saveImg
        self.hub = hubclient.Hub(beamline.lower())

        # Define other workers we will use
        self.center_on_tip_job = centerOnTip.centerOnTip(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            saveImg=False,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
        )

        self.find_flat_face_job = findFlatFace.findFlatFace(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            saveImg=False,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
        )

        self.find_bounding_box_job = findLoopBoundingBox.findLoopBoundingBox(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
            saveImag=True,
        )

        self.find_loop_thickness_job = findLoopThickness.findLoopThickness(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            setupSamCam=False,
            restoreSamCam=False,
            apply_finishing_touches=False,
            saveImg=True,
        )

        super(centerToFlat, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def worker(self):
        """Rotates omega along the angles given in omegaSteps, and use sin_fit to find biggest area.
        # ESCAPE STATE: SAMPLE ALIGNMENT
        """

        logger.info("Setting Sample Camera to fixed gain and exposure")

        if self.debug:
            start = time.time()
        self.samCam.setup()

        if self.debug:
            logger.info("Camera setup time: {}".format(time.time() - start))
            print("centerToFlat:Camera setup time: {}".format(time.time() - start))

        # Get image before centering on tip
        img = self.samCam.getImage(gray=False)
        now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        procImg = lip(img)
        procImg.saveImage(
            "/scratch/alc_data/alc_images/images/beforeCenterOnTip",
            filename="{}.jpg".format(now),
        )

        # Find tip
        logger.info("centerToFlat: Executing CenterOnTip worker")
        if self.debug:
            start = time.time()
        if self.is_aborted():
            return {}
        self.center_on_tip_job.worker()
        if self.debug:
            logger.info("centerToFlat: centerOnTip time: {}".format(time.time() - start))
            print("centerToFlat: centerOnTip time: {}".format(time.time() - start))

        # Zoom in
        logger.info("centerToFlat: zooming to flatFaceZoom")
        if self.debug:
            start = time.time()
        if self.is_aborted():
            return {}
        self.sampleMotor.zoomto(-208)
        if self.debug:
            logger.info("centerToFlat: zooming time: {}".format(time.time() - start))
            print("centerToFlat: zooming time: {}".format(time.time() - start))

        # Center to flat
        logger.info("centerToFlat: finding flat face")
        if self.debug:
            start = time.time()
        if self.is_aborted():
            return {}
        self.find_flat_face_job.worker()
        if self.debug:
            logger.info("centerToFlat: flatFace time: {}".format(time.time() - start))
            print("centerToFlat: flatFace time: {}".format(time.time() - start))

        # Find loop thickness here
        logger.info("Finding loop thickness")
        omega = self.omegaMotor.getOmega()
        self.omegaMotor.moveby(-90)
        if self.is_aborted():
            return {}
        thickness = self.find_loop_thickness_job.worker()
        self.omegaMotor.moveby(90)

        if self.is_aborted():
            return {}
        if self.end_with_center_on_tip:
            flat_omega = self.omegaMotor.getOmega()
            tip_position = self.center_on_tip_job.worker()
            self.omegaMotor.moveto(flat_omega)
            tip_position["omega"] = flat_omega

        logger.info("I am sleeping after 90 degree rotation")
        time.sleep(1.0)

        # IMPORTANT: Update tip_position to gonio-beamline coordinates for raster grid generation in SDU
        # IMPORTANT: This needs to be before final movement as a reference point for beam position in pixels
        tip_position["motor_x_at_tip"] = self.sampleMotor.blxrbv.getw()  # tip position for grid scan
        tip_position["motor_y_at_tip"] = self.sampleMotor.blyrbv.getw()
        tip_position["motor_z_at_tip"] = self.sampleMotor.blzrbv.getw()
        logger.info(
            "tip_position is {}, {}, {}".format(
                tip_position["motor_x_at_tip"],
                tip_position["motor_y_at_tip"],
                tip_position["motor_z_at_tip"],
            )
        )

        logger.info("Moving the sample tip by {}".format(-1000 * self.hub.getd("alc_final_movement_mm")))
        move = [self.hub.getd("alc_final_movement_mm"), 0.0]

        # Save image before "finishing touches when the loop is moved inwards
        # TODO temporary to acquire some images for deep learning. Remove it later
        img = self.samCam.getImage(gray=False)
        now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        procImg = lip(img)
        procImg.saveImage(
            "/scratch/alc_data/alc_images/images/flatFace",
            filename="{}.jpg".format(now),
        )

        omega = self.omegaMotor.getOmega()
        moveXY = bu.xy2xyz(self.beamline, move, omega)

        if self.is_aborted():
            return {}
        self.sampleMotor.moveby_in_cryozone(moveXY[0], moveXY[1], moveXY[2])
        time.sleep(1.0)  # wait until after motion is finalised

        # Find bounding box here
        if self.is_aborted():
            return {}
        box = self.find_bounding_box_job.worker()
        logger.info("centerToFlat: Found bounding box: {}".format(box))

        if self.hub.get("sdu_activated") in ["false", "False", False]:
            self.sampleMotor.zoomto(-10)
        self.samCam.restore()

        return dict(grid_corners=box, tip_position=tip_position, thickness=thickness)
