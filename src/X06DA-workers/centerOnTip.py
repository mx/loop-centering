#!/usr/bin/env python

import beamlineUtils as bu
import globalStates as state
import hubclient
import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

logger = logging.getLogger("Worker-centerOnTip")


class centerOnTip(workerBaseClass.workerBaseClass):
    """Centers beam on the tip of the loop"""

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        relativeOmega=[0, 90],
        setupSamCam=True,
        restoreSamCam=True,
        debug=False,
        saveImg=True,
        apply_finishing_touches=True,
        *args,
        **kwargs,
    ):

        self.relativeOmega = relativeOmega
        self.setupSamCam = setupSamCam
        self.restoreSamCam = restoreSamCam
        self.debug = debug
        self.saveImg = saveImg
        self.hub = hubclient.Hub(beamline.lower())
        self.apply_finishing_touches = apply_finishing_touches

        # Parameter to cut out the border from top and the bottom of the picture
        # Default: 110 - its around the half-width of the pin mounting the loop at zoom -500.0
        self.envelope_y = kwargs.pop("envelope_y", 110)

        super(centerOnTip, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def tip2beam(self, wait=True, debug=False):
        """Finds the tip of the loop and moves it to the beamposition
        wait: if set to True, wait until the sample motor movement is finished
        debug: if True, will show the images with the contour and the tip"""

        zoom = self.sampleMotor.currentZoom()
        omega = self.omegaMotor.getOmega()

        logger.info("Aligning tip to beam for Omega={} at zoom={}".format(omega, zoom))
        # Find beam position at current zoom level
        beamPos = bu.findBeamPosition(self.beamline, zoom)
        # Get image from camera
        img = self.samCam.getImage(gray=False)
        # Process image
        procImg = lip(img)
        procImg.findContour(zoom=zoom, beamline=self.beamline, hull=True)
        # Find the tip of the loop on processed image
        tip = procImg.findTip()
        # Find how close to the edge is the tip
        fullMove = True
        if (tip[1] < self.envelope_y) or (
            tip[1] > img.shape[1] - self.envelope_y
        ):  # Sample to close to the top or bottom edge of the image.
            logger.info(
                "Tip detected suspiciously close to the top or bottom Y-edge of the image. Will move it first along Y towards the beam."
            )
            fullMove = False  # forces movement only along Y-axis
        # Save the image in the log directory
        if self.saveImg:
            procImg.saveContour(directory="/scratch/alc_data/alc_images/images/zoom208-contours")
        if self.debug:
            procImg.showTip()
        # Find how far is the tip from the beam
        delta = self.sampleMotor.pixel_to_mm(beamPos - tip)
        # If sample os to close to the edge, move it only along Y axis
        if not fullMove:
            delta[0] = 0.0  # don't move along X
        # Find how to move the prigo, depending on the value of Omega
        moveXY = bu.xy2xyz(self.beamline, delta, omega)
        # Move the sample Motor
        logger.info("Moving Sample Motor by {} {} {}".format(moveXY[0], moveXY[1], moveXY[2]))
        self.sampleMotor.moveby_in_cryozone(moveXY[0], moveXY[1], moveXY[2])
        if wait:
            self.sampleMotor.wait()

        return fullMove  # returns True/False

    def finishing_touches(self, save_img=False, save_image_dir=None):
        # Move the tip by the 90um to the right
        logger.info("Moving the sample tip by 90um")
        move = [-0.09, 0.0]
        omega = self.omegaMotor.getOmega()
        moveXY = bu.xy2xyz(self.beamline, move, omega)
        self.sampleMotor.moveby_in_cryozone(moveXY[0], moveXY[1], moveXY[2])

        # Zoom after finish
        logger.info("Going to the end zoom")
        endZoom = self.hub.getd("alc_zoom_at_end")
        self.sampleMotor.zoomto(endZoom)
        # Take an image after end zoom is reached
        if self.saveImg or save_img:
            logger.info("Saving image after final zoom was reached")
            img = self.samCam.getImage(gray=False)
            procImg = lip(img)
            if save_image_dir is None:
                # procImg.saveImage(directory='/home/logs/alc/images/zoom10')
                procImg.saveImage(directory="/scratch/alc_data/alc_images/images/zoom208")
            else:
                procImg.saveImage(directory=save_image_dir)

    def worker(self):
        """Centers loop on the Tip for given relative omega angles.
        Calls tip2beam for each Omega
        # ESCAPE STATE: SAMPLE ALIGNMENT
        """
        # Make sure omega angle is iterable.

        if not isinstance(self.relativeOmega, list):
            self.relativeOmega = list(self.relativeOmega)

        if self.setupSamCam:
            logger.info("Setting Sample Camera to fixed gain and exposure")
            self.samCam.setup()

        for i, omegaInc in enumerate(self.relativeOmega):
            # Check if user aborted operation
            if self.stop():
                self.samCam.restore()
                self.result = workerRuntime.workerResultAbort
                logger.info("centerOnTip aborted")
                return

            self.omegaMotor.moveby(omegaInc)
            omega = self.omegaMotor.getOmega()
            waitForSampleMotor = True
            # If we do the last centering, release Sample Motor earlier
            if i == len(self.relativeOmega):
                waitForSampleMotor = False

            fullMove = self.tip2beam(wait=waitForSampleMotor, debug=self.debug)

            if not fullMove:  # Means sample was too close to the border and was moved only along Y-axis
                logger.info("Recentering on the tip again...")
                self.sampleMotor.wait()  # Wait for previous movement to be finished
                fullMove = self.tip2beam(wait=waitForSampleMotor, debug=self.debug)

        if self.restoreSamCam:
            logger.info("Restoring sample camera to automatic gain and exposure")
            self.samCam.restore()

        # Get current motor positions indicating gonio_beamline when tip of loop is in the beam box coordinates
        motor_x = self.sampleMotor.xrbv.get()
        motor_z = self.sampleMotor.yrbv.get()
        motor_y = self.sampleMotor.zrbv.get()
        omega = self.omegaMotor.getOmega()

        # Tip of the loop is in the beambox - return this position - will be reference for rasterGridGenerator
        zoom = self.sampleMotor.currentZoom()
        beamPos = bu.findBeamPosition(self.beamline, zoom)

        # Finishing touches - Move the tip by the 90um to the right
        if self.apply_finishing_touches:
            self.finishing_touches()

        return dict(
            motor_x_at_tip=motor_x,
            motor_y_at_tip=motor_y,
            motor_z_at_tip=motor_z,
            omega=omega,
            tip_x=beamPos[0],
            tip_y=beamPos[1],
        )
