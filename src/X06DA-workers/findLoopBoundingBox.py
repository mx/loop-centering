#!/usr/bin/env python

import beamlineUtils as bu
import globalStates as state
import hubclient
import logging
import numpy
import scipy.optimize
import time
import workerBaseClass
import workerRuntimeStates as workerRuntime
from loopImageProcessing import loopImageProcessing as lip

logger = logging.getLogger("Worker-findLoopBoundingBox")


class findLoopBoundingBox(workerBaseClass.workerBaseClass):
    """Set the loop in the microscope to face with biggest area"""

    def __init__(
        self,
        beamline,
        motors,
        redis_host,
        redis_port,
        redis_db,
        setupSamCam=True,
        restoreSamCam=True,
        apply_finishing_touches=True,
        debug=False,
        saveImage=True,
        *args,
        **kwargs,
    ):
        """

        :param beamline:
        :param motors:
        :param redis_host:
        :param redis_port:
        :param redis_db:
        :param setupSamCam:
        :param restoreSamCam:
        :param apply_finishing_touches:
        :param debug:
        :param args:
        :param kwargs:
        """

        self.setupSamCam = setupSamCam
        self.restoreSamCam = restoreSamCam
        self.apply_finishing_touches = apply_finishing_touches
        self.debug = debug
        self.saveImg = saveImage
        self.hub = hubclient.Hub(beamline.lower())

        super(findLoopBoundingBox, self).__init__(
            beamline=beamline,
            motors=motors,
            redis_host=redis_host,
            redis_port=redis_port,
            redis_db=redis_db,
            *args,
            **kwargs,
        )

    def findLoopBoundingBox(self):
        zoom = self.sampleMotor.currentZoom()

        img = self.samCam.getImage(gray=False)

        procImg = lip(img)
        procImg.saveImage(directory="/scratch/alc_data/alc_images/images/prebbox/")  # Image before bounding box applied
        procImg.findContour(zoom=zoom, beamline="params{}bounding".format(self.beamline), hull=False)
        box = procImg.findLoopBoundingBox()
        if self.saveImg:
            procImg.showLoopBoundingBox(save_img=True, directory="/scratch/alc_data/alc_images/images/bbox/")

        # Box is dict with numpy arrays. Convert to lists to make it easier to json serialize
        for i in box:
            box[i] = box[i].tolist()
        return box

    def finishing_touches(self):
        # Finishing touches #
        pass  # Nothing to be done

    def worker(self):
        """

        :return:
        """

        if self.setupSamCam:
            logger.info("Setting Sample Camera to fixed gain and exposure")
            self.samCam.setup()

        # Find bounding box and save it to an images
        img_proc_start = time.time()
        box = self.findLoopBoundingBox()
        img_proc_stop = time.time()

        if self.debug:
            logger.info("Find bounding box Processing: {}".format(img_proc_stop - img_proc_start))

        if self.apply_finishing_touches:
            self.finishing_touches()

        if self.restoreSamCam:
            logger.info("Restoring sample camera to automatic gain and exposure")
            self.samCam.restore()

        return box
