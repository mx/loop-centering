#!/usr/bin/env python

# THIS IS USED BY DOCKER IMAGE BUILDER

# Simple parser script to convert platform dependent (OSX, linux-64) list of
# pacakges given in environment.yml file used by Anaconda Python to platform
# independent case.
#
# User need to prepare list of main packages that were requested in the
# environment, without specyfing their dependencies. It is the package
# dependecies that makes environment.yml platform dependend as for instance
# ipyton on OSX has different dependencies than ipython on Linux-64
#
# The environment.yml is given as command line argument
#
# The script creates two files
# 1. os_independent_environment.txt - used with command
#    conda create --copy --name=<envName> --file os_independent_environment.txt
#
#    It will create environment with the requested pakages and its dependencies.
#    NOTE: using truncated .yml file does not work, as conda does not look
#    and install package dependecies.
#
# 2.  requirements.txt used by pip install -r requirements.txt
#
# The latter command is issued inside environment created in 1.
# (i.e. after source activate <environment-name>)
#
# The reason for this division is that in some cases it failed to create
# environment with "conda create -f <file>" when <file> has listed pip
# installed packages

import sys

packages = [
    "ipython",
    "opencv-python",
    "flask",
    "pymongo",
    "pandas",
    "stomp.py",
    "flask-pymongo",
    "loguru",
    "pip",
    "numpy",
    "python",
    "plotly",
    "tornado",
    "gevent",
    "gunicorn",
    "redis",
    "redis-py",
    "cachannel",
    "requests",
]

envYML = sys.argv[1]

# Packagas as pip and python are always installed by default when creating
# environment. Make sure they are on the list of packages to check in order to
# ensure that the same version number is installed
if "pip" not in packages:
    packages.append("pip")
if "python" not in packages:
    packages.append("python")

# Create empty lists when the found packages and versions will be stored.
conda = []
pip = []

# Loop pver package list
for package in packages:
    # Format package to be '- <package>='
    package = "- %s=" % package
    file = open(envYML, "r")
    name = file.readline()  # Read first two lines of .yml file
    deps = file.readline()
    pipAdd = False
    # for each package go over environment file trying to find its version
    # number. Poor man solution, but get the job done.
    while 1:
        line = file.readline()
        line = line.strip()
        if line == "":
            break
        if not pipAdd:
            if line == "- pip:":  # we want to separate pip packages from conda
                pipAdd = True
            else:
                if package in line:
                    conda.append(line)
        else:
            if package in line:
                pip.append(line)
    file.close()

file = open("os_independent_environment.txt", "w")
# Write heders - name of the environment and dependencies keyword
# file.write(name)
# file.write(deps)
for package in conda:
    package = package[2:]
    # Want to remove =py27 from package name, for instance  pip=7.1.2=py27_0
    # It sometimes causes dependecies not be installed
    package = package.split("=")
    package = package[0] + "=" + package[1] + "\n"
    file.write(package)
file.close()
# Write requirements.txt file that is used by pip
file = open("requirements.txt", "w")
for package in pip:
    package = package[2:]
    file.write(package + "\n")
file.close()
