### Anaconda environment for loop-centering package.

* environment.yml = current python3.9 production environment 

The environment.yml is sometimes prepared on MacOS and it is not portable to Linux.

To use this environment on Linux, use envParser.py script first to produce
system independent list of packages.

**Note**
The environment.yml and envParser.py need to be maintained. Each time new package is
added to the environment, new environment.yml needs to be created:
```
$ conda env export > environment.yml
```
The package name also needs to be aded to package list in envParser.py by hand.

### Usage:
  * MacOS

    ```
    $ conda env create -f environment.yml
    ```
  * Linux

    ```
    $ ./envParser.py environment.yml
    $ conda create --name mongo --file os_independent_environment.txt
    $ pip install -r requirements.txt
    ```
