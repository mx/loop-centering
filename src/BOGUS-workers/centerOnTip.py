#!/usr/bin/env python

import logging
import sys
import workerBaseClass

sys.path.insert(0, "../lib")
import globalStates as state
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-centerOnTip")


class centerOnTip(workerBaseClass.workerBaseClass):
    """Centers beam on the tip of the loop"""

    def __init__(
        self,
        beamline,
        redis_host,
        redis_port,
        redis_db,
        relativeOmega=[0, 90],
        restoreSamCam=True,
        debug=False,
        *args,
        **kwargs,
    ):
        self.relativeOmega = relativeOmega
        self.restoreSamCam = restoreSamCam
        self.debug = debug
        super(centerOnTip, self).__init__(
            beamline=beamline, redis_host=redis_host, redis_port=redis_port, redis_db=redis_db, *args, **kwargs
        )

    def tip2beam(self, wait=True, debug=False):
        """Finds the tip of the loop and moves it to the beamposition
        wait: if set to True, wait until the sample motor movement is finished
        debug: if True, will show the images with the contour and the tip"""

        zoom = self.sampleMotor.currentZoom()
        omega = omegaMotor.getOmega()

        logger.info("Aligning tip to beam for Omega={} at zoom={}".format(omega, zoom))
        # Find beam position at current zoom level
        beamPos = bu.findBeamPosition(self.beamline, zoom)
        # Get image from camera
        img = samCam.getImage(gray=False)
        # Process image
        procImg = lip(img, self.beamline, zoom)
        # Find the tip of the loop on processed image
        tip = procImg.findTip()
        if debug:
            procImg.showTip()
        # Find how far is the tip from the beam
        delta = sampleMotor.pixel_to_mm(beamPos - tip)
        # Find how to move the prigo, depending on the value of Omega
        moveXY = bu.xy2xyz(delta, omega)
        # Move the sample Motor
        logging.info("Moving Sample Motor by {} {} {}".format(moveXY[0], moveXY[1], moveXY[2]))
        sampleMotor.moveby(moveXY[0], moveXY[1], moveXY[2])
        if wait:
            sampleMotor.wait()

    def worker(self):
        """Centers loop on the Tip for given relative omega angles.
        Calls tip2beam for each Omega"""
        # Make sure omega angle is iterable.

        if not isinstance(self.relativeOmega, list):
            self.relativeOmega = list(self.relativeOmega)

        logger.info("Setting Sample Camera to fixed gain and exposure")
        self.samCam.setup()

        for i, omegaInc in enumerate(self.relativeOmega):
            # Check if user aborted operation
            if self.stop():
                self.result = state.workerResultAbort
                logging.info("centerOnTip aborted")
                return

            # TODO: check for Omega limit and check if movement is allowed, abort otherwise
            omegaMotor.moveby(omegaInc)
            omega = omegaMotor.getOmega()
            waitForSampleMotor = True
            # If we do the last centering, release Sample Motor earlier
            if i == len(relativeOmega):
                waitForSampleMotor = False

            self.tip2beam(wait=waitForSampleMotor, debug=self.debug)

        if self.restoreSamCam:
            logger.info("Restoring sample camera to automatic gain and exposure")
            self.samCam.restore()
