#!/usr/bin/env python

import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-testSetup")


class testSetup(workerBaseClass.workerBaseClass):
    """Prepares the beamline to the state as it was mounted by robot. Used in development"""

    def __init__(self, beamline, redis_host, redis_port, redis_db, *args, **kwargs):
        super(testSetup, self).__init__(
            beamline=beamline, redis_host=redis_host, redis_port=redis_port, redis_db=redis_db, *args, **kwargs
        )

    def worker(self):
        p = Prigo.Prigo()
        p.move_home()
        p.wait()
        print
        "Prigo Homed"
        zoom = -500
        omegaMotor.moveto(90)
        sampleMotor.zoomto(zoom)
        logger.info("Beamline in Cats mount-like setup.")
