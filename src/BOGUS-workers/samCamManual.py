#!/usr/bin/env python

import logging
import workerBaseClass
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-samCamManual")


class samCamManual(workerBaseClass.workerBaseClass):
    """Prepares the beamline to the state as it was mounted by robot. Used in development"""

    def __init__(self, beamline, redis_host, redis_port, redis_db, *args, **kwargs):
        super(samCamManual, self).__init__(
            beamline=beamline, redis_host=redis_host, redis_port=redis_port, redis_db=redis_db, *args, **kwargs
        )

    def worker(self):
        self.samCam.setup()
