#!/usr/bin/env python

import logging
import time
import workerBaseClass
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-Bogus2")


class bogus2(workerBaseClass.workerBaseClass):
    def __init__(self, beamline, redis_host, redis_port, redis_db, *args, **kwargs):
        super(bogus2, self).__init__(
            beamline=beamline, redis_host=redis_host, redis_port=redis_port, redis_db=redis_db, *args, **kwargs
        )

    def worker(self):
        """Do some works"""
        logger.info("Running bogus worker!!!")
        i = 0
        f = open("bog2.txt", "w")
        f.write("Start\n")
        f.close()
        test = self.kwargs.pop("test", "Nothing")

        while i < 10:
            if self.stop():
                self.result = workerRuntime.workerResultAbort
                return
            print
            "Bogus2 ", i
            print
            "Stop ", self.stop()
            print
            "Kwargs", test
            f = open("bog2.txt", "a")
            f.write("Bogus %i\n" % i)
            f.close()
            i += 1
            time.sleep(0.1)

        self.result = workerRuntime.workerResultSuccess
