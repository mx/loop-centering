#!/usr/bin/env python

import logging
import time
import workerBaseClass
import workerRuntimeStates as workerRuntime

logger = logging.getLogger("Worker-Bogus")


class bogus(workerBaseClass.workerBaseClass):
    def __init__(self, beamline, redis_host, redis_port, redis_db, *args, **kwargs):
        super(bogus, self).__init__(
            beamline=beamline, redis_host=redis_host, redis_port=redis_port, redis_db=redis_db, *args, **kwargs
        )

    def worker(self):
        """Do some works"""
        # Put everything in try-except block, to make sure that if something fails, theard exits normally and Busy is put to False
        logger.info("Running bogus worker!!!")
        i = 0
        while i < 10 and not self.stop():
            print
            i
            print
            self.stop()
            i += 1 / 0
        time.sleep(20)
