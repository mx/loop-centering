from flask import Blueprint, jsonify, request
from app.api import common, error_handler
from app.api.error_handler import InvalidUsage

import os

import redis

from app import g, alc, state, workerRuntime, app

api = Blueprint("api_v7", __name__)


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@api.route("/")
def index():
    return common.commonIndex()


@api.route("/abort", methods=["POST"])
def abort():
    app.logger.info("Requesting ABORT")
    r = g.redis
    d = {}

    if r.get(state.abort).decode("utf-8") == state.abortTrue:
        d["status"] = "ABORT already in progress. Canceling request"
        app.logger.info(d["status"])
        return jsonify(**d), 202
        # raise InvalidUsage(msg, status_code=409)

    with r.pipeline() as pipe:
        try:
            pipe.watch(state.abort)
            pipe.watch(state.busy)  # WATCH BUSY and ABORT states, to make sure they have not changed in the meanwhile
            if pipe.get(state.busy) == state.busyTrue:  # Try to set ABORT to True if ALC is in BUSY state
                pipe.multi()
                pipe.set(state.abort, state.abortTrue)
                pipe.execute()
            else:
                d["status"] = "ALC worker is not running. Nothing to ABORT."
                app.logger.info(d["status"])
                return jsonify(**d), 202
                # raise InvalidUsage(msg, status_code=404)
        except redis.WatchError:
            d[
                "status"
            ] = "WatchError. Value of ABORT was changed from somewhere else, when command was being issued. Canceling current request"
            app.logger.info(d["status"])
            return jsonify(**d), 202
            # raise InvalidUsage(msg, status_code=409)

    d["status"] = "State changed to ABORT"
    app.logger.info(d["status"])
    return jsonify(**d)


@api.route("/status")
def status():
    r = g.redis
    d = {}

    d["busy"] = r.get(state.busy).decode("utf-8")

    if d["busy"] == state.busyTrue:
        d["busy"] = True
        d["name"] = r.get(state.currentWorker).decode("utf-8")
        d["result"] = "None"
        d["error"] = "None"
        d["data"] = []
    else:
        d["busy"] = False
        d["name"] = r.get(workerRuntime.workerName).decode("utf-8")
        d["result"] = r.get(workerRuntime.workerResult).decode("utf-8")
        d["error"] = r.get(workerRuntime.workerErrorMsg).decode("utf-8")
        d["data"] = r.get(workerRuntime.workerReturnData).decode("utf-8")

    return jsonify(**d)


@api.route("/busy")
def busy():
    r = g.redis
    d = {}

    d["busy"] = r.get(state.busy).decode("utf-8")

    if d["busy"] == state.busyTrue:
        d["busy"] = True
    else:
        d["busy"] = False

    return jsonify(**d)


@api.route("/run/<worker_name>", methods=["POST"])
def run(worker_name):
    kwargs = request.args.to_dict()
    d = {}
    try:
        success = alc.run(workerName=worker_name, **kwargs)
        if success:
            d["status"] = "OK"
            return jsonify(**d)
        else:
            d["status"] = "Other worker process is already running. Can not run another process."
            return jsonify(**d), 202

    except Exception as e:
        msg = "Could not run worker {}. Reason: {}".format(worker_name, e)
        app.logger.info(msg)
        raise InvalidUsage(msg, status_code=409)


@api.route("/list", methods=["GET"])
def listWorkers():
    d = {}
    workers = alc.listWorkers()
    # d['status'] = 'Available workers: {}'.format(workers)
    d["status"] = workers
    return jsonify(**d)


@api.route("/restart", methods=["POST"])
def restart():
    # Restarts the whole server. As a result, if there is any hanging thread, is it also killed!
    import os, signal

    d = {}

    try:
        pidFile = os.environ["ALC_PID_FILE"]
    except KeyError:
        pidFile = "alc.pid"

    with open(pidFile, "r") as f:
        pid = int(f.readline())
        f.close()
    app.logger.info("RESTARTING THE WHOLE APPLICATION! PID FILE: {}".format(pidFile))
    os.kill(pid, signal.SIGHUP)
    d["status"] = "OK"
    return jsonify(**d)
