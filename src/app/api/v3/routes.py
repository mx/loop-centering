from flask import Blueprint, jsonify, request
from app.api import common, error_handler
from app.api.error_handler import InvalidUsage

import redis

from app import g, alc, state, workerRuntime, app

api = Blueprint("api_v3", __name__)


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@api.route("/")
def index():
    return common.commonIndex()


@api.route("/abort", methods=["POST"])
def abort():
    app.logger.info("Requesting ABORT")
    r = g.redis

    if r.get(state.abort) == state.abortTrue:
        msg = "ABORT already in progress. Canceling request"
        app.logger.info(msg)
        return msg, 202
        # raise InvalidUsage(msg, status_code=409)

    with r.pipeline() as pipe:
        try:
            pipe.watch(state.abort)
            pipe.watch(state.busy)  # WATCH BUSY and ABORT states, to make sure they have not changed in the meanwhile
            if pipe.get(state.busy) == state.busyTrue:  # Try to set ABORT to True if ALC is in BUSY state
                pipe.multi()
                pipe.set(state.abort, state.abortTrue)
                pipe.execute()
            else:
                msg = "ALC worker is not running. Nothing to ABORT."
                app.logger.info(msg)
                return msg, 202
                # raise InvalidUsage(msg, status_code=404)
        except redis.WatchError:
            msg = "WatchError. Value of ABORT was changed from somewhere else, when command was being issued. Canceling current request"
            app.logger.info(msg)
            return msg, 202
            # raise InvalidUsage(msg, status_code=409)

    msg = "State changed to ABORT"
    app.logger.info(msg)
    return msg


@api.route("/busy")
def busy():
    r = g.redis
    d = {}

    d["busy"] = r.get(state.busy)

    if d["busy"] == state.busyTrue:
        d["name"] = r.get(state.currentWorker)
        d["result"] = "None"
        d["error"] = "None"
    else:
        d["name"] = r.get(workerRuntime.workerName)
        d["result"] = r.get(workerRuntime.workerResult)
        d["error"] = r.get(workerRuntime.workerErrorMsg)

    return jsonify(**d)


@api.route("/run/<worker_name>", methods=["POST"])
def run(worker_name):
    # TODO: implement args and kwargs to be passed to alc.run()
    kwargs = request.args.to_dict()
    try:
        success = alc.run(workerName=worker_name, **kwargs)
        if success:
            return "OK"
        else:
            msg = "Other worker process is already running. Can not run another process."
            return msg, 202
    except Exception as e:
        msg = "Could not run worker {}. Reason: {}".format(worker_name, e)
        app.logger.info(msg)
        raise InvalidUsage(msg, status_code=409)


@api.route("/list", methods=["GET"])
def listWorkers():
    workers = alc.listWorkers()
    msg = "Available workers: {}".format(workers)
    return msg


@api.route("/restart", methods=["POST"])
def restart():
    # Restarts the whole server. As a result, if there is any hanging thread, is it also killed!
    import os, signal

    pidFile = "g.pid"
    with open(pidFile, "r") as f:
        pid = int(f.readline())
        f.close()
    app.logger.info("RESTARTING THE WHOLE APPLICATION!")
    os.kill(pid, signal.SIGHUP)
    return "OK"
