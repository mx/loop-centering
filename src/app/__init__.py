#!/usr/bin/env python
import logging
import os
import redis
import sys
from flask import Flask, g
from logging.handlers import RotatingFileHandler

# Set up alc module
sys.path.insert(0, "lib")
sys.path.insert(0, "etc")
sys.path.insert(0, "app")

from AlcDriver import AlcDriver
import globalStates as state
import workerRuntimeStates as workerRuntime
import beamlineUtils as bu

import redis_conf

app = Flask("__name__")

app.logger_name = "flask.app"
datefmt = "%Y-%m-%d %H:%M:%S"
formatter = logging.Formatter(
    "%(asctime)s [%(process)d:%(name)s:%(lineno)s] [%(levelname)s] %(message)s",
    datefmt=datefmt,
)
# handler = RotatingFileHandler('gunicorn.log', maxBytes=10000000, backupCount=5)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
handler.setFormatter(formatter)
app.logger.addHandler(handler)

beamline = os.getenv("BEAMLINE_XNAME", "bogus").upper()

try:
    beamline = bu.validateBeamline(beamline)
except Exception as e:
    msg = "Cannot find beamline name: {}".format(e)
    app.logger.info(msg)
    raise Exception(msg)

app.logger.info("Running on beamline {}".format(beamline))

# This makes alc global
try:
    alc = AlcDriver(beamline)
except Exception as e:
    raise Exception("Something wrong with ALC module initialization, ", e)

app.logger.info("Setting worker path to {}-workers".format(beamline))
sys.path.insert(0, "{}-workers".format(beamline))


@app.before_request
def init_redis():
    redis_host = redis_conf.redis_host
    redis_port = redis_conf.redis_port
    redis_db = redis_conf.redis_db[beamline]
    # app.logger.info('Connecting to REDIS at {}:{}'.format(redis_host, redis_port))
    # Always start fresh connection to REDIS upon request
    # TODO: throw error and quit when REDIS is not available
    r = redis.StrictRedis(host=redis_host, port=redis_port, db=redis_db)
    try:
        t = r.get("test")  # try to get some value. It will fail if REDIS is not available.
    except redis.ConnectionError:
        msg = "Connection to REDIS at {}:{} FAILED!".format(redis_host, redis_port)
        app.logger.info(msg)
        return msg, 503
    g.redis = r


# TODO: Do we need app.teardown to release REDIS?

from api.v9.routes import api as api_v9

app.register_blueprint(api_v9)
# app.register_blueprint(api_v2, url_prefix='/v2')
